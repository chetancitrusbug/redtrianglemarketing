@extends('layouts.backend')

@section('title',trans('document.documents'))
@section('pageTitle',trans('document.documents'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('document.documents')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ url('/admin/documents/create') }}" class="btn btn-success btn-sm"
                                   title="Add New Document">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('document.add_new_document')
                                </a>

                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/documents', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}

                            {!! Form::close() !!}
                            </div>
                        </div>



                    <div class="table-responsive">
                        <table class="table table-borderless" id="documents-table">
                            <thead>
                            <tr>
                               <!-- <th data-priority="1">@lang('document.id')</th>-->
                                <th data-priority="3">@lang('document.title')</th>
                                <th data-priority="5">@lang('document.document_url')</th>
                                <th data-priority="7">@lang('document.status')</th>
                                <th data-priority="8">@lang('document.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/documents/') }}";
var img_path ="{{ url('Document/') }}";

        datatable = $('#documents-table').DataTable({
             //"order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('DocumentControllerDocumentsData') !!}',
                    type: "get", // method , by default get

                },
                columns: [
                   // { data: 'id', name: 'Id',"searchable": false },
                    { data: 'title',name:'documents.title',"searchable" : false},
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {

							var img=o.document_url;
							if(img){
                                var param  = img_path+'/'+o.document_url;
                                var val = checkImage(param);
                                if(val == 0 || val == null)
                                {
                                    return 'No Document';
                                }
                                else
                                {
                                   return '<a href="'+param+'" target="_blank">Click here</a>';
                                }

                            }else{
								return 'No Document';
							}
                        }
			        } ,
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status=='blocked')
                            status = "<a href='"+url+"/"+o.id+"?status=blocked' data-id="+o.id+" title='blocked'><button class='btn btn-default btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('document.blocked')</button></a>";
                            else if(o.status == 'inactive')

                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("document.inactive")</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('document.active')</button></a>";

                            return status;

                        }

                    },
					{
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";

                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";

                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('document.view')</button></a>&nbsp;";


                            return v+e+d;
                        }
                    }

                ]
        });

    function checkImage(url) {
        status = 0;
        $.ajax({
            url: url,
            async: false,
            error: function()
            {
                status =  0;
            },
            success: function(url)
            {
                status =  1;
            }
        });
        console.log(status);
        return status;
    }
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/documents/') }}";
        url = url + "/" + id;
		//alert(url);
        var r = confirm("Are you sure you want to delete Document ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

</script>
@endpush
