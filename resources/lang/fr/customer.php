<?php

return [

	'customers' => 'Les clients',
	'add_new' => 'Ajouter de nouvelles',
	'id' => 'ID',
	'name' => 'Nom',
	'email' => 'E-mail',
	'phone' => 'Téléphone',
	'mobile' => 'Mobile',
	'cpnj' => 'CPNJ',
	'address' => 'Répondre',
	'city' => 'Ville',
	'state' => 'State',
	'country' => 'Pays',
	'zip' => 'Zip',
	'operator' => 'Opérateur',
	'supplier' => 'Fournisseur',
	'actions' => 'Actions',
	'edit' => 'Modifier',
	'view' => 'Voir',
	'delete' => 'Supprimer',
	'create_customer' => 'Créer un client',
	'create_new_customer' => 'Créer un nouveau client',
	'back' => 'Retour',
	'edit_customer' => 'Modifier un client',
	'address_number' => 'Address Number',
	'address_street_name' => 'Address Street Name',
	'address_complement' => 'Address Complement',
	'neighborhood' => 'Neighborhood',
	'website' => 'Website',
	'legal_name' => 'Legal Name',
	'type' => 'Type',


];