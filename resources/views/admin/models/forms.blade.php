<div class="panel panel-default">
    <div class="panel-heading">Geothermal Checklist</div>
    <div class="panel-body">
        <a href="#" title="Back to Forms list" ><button type="button" class=" backToForm btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back</button></a>
        {!! Form::hidden('job_id', $job->id, ['class' => 'form-control input-100']) !!}
        <div class="box-1 clearfix">

            <div class="col-md-12 col-sm-12">
                <h3 class="h3-title">1. JOB INFORMATION</h3>
            </div>

            <div class="col-md-4 col-sm-4 clearfix">
                <p class="height-auto">Model #:</p>
                {!! Form::text('model', null, ['class' => 'form-control input-100']) !!}
                {!! $errors->first('model', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-4 col-sm-4 clearfix">
                <p class="height-auto">Job Name:</p>
                {!! Form::text('job_name', $job->title, ['class' => 'form-control input-100','readonly']) !!}
                {!! $errors->first('job_name', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-4 col-sm-4 clearfix">
                <p class="height-auto">Open/Closed Loop:</p>
                {!! Form::text('open_closed_loop', null, ['class' => 'form-control input-100']) !!}
                {!! $errors->first('open_closed_loop', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-4 col-sm-4">
                <p class="height-auto">Serial #:</p>
                {!! Form::text('serial', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('serial','<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-4 col-sm-4">
                <p class="height-auto">Install Date:</p>
                {!! Form::text('install_date', null, ['id' => 'install_date','class' => 'form-control input-100']) !!}  {!! $errors->first('install_date','<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-md-4 col-sm-4">
                <p class="height-auto">Desuperheater (Y/N):</p>
                {!! Form::select('desuperheater',['N'=>'N','Y'=>'Y'], isset($jobGeothermalForm->desuperheater)?$jobGeothermalForm->desuperheater:null, ['class' => 'form-control'])
                !!}
                {!! $errors->first('desuperheater','<p class="help-block">:message</p>') !!}
            </div>

        </div>
        <!-- End of box-div -->

        <div class="box-1 box-2 clearfix">

            <div class="col-md-12 col-sm-12">
                <h3 class="h3-title">2. FLOW RATE IN GPM</h3>
            </div>

            <div class="col-md-3 col-sm-3 clearfix">
                <p class="height-auto"></p>
                <p class="height-auto">WATER IN Pressure:</p>
                <p class="height-auto">WATER OUT Pressure:</p>
                <p class="height-auto">Pressure Drop = a - b:</p>
                <p class="height-auto">Look up flow rate in table:</p>
            </div>
            <div class="col-md-3 col-sm-3 clearfix">
                <p class="height-auto clearfix">SOURCE COAX </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">a.</span>
                    {!! Form::text('source_coax_a', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('source_coax_a','<p class="help-block">:message</p>') !!}
                <span class="block-span">PSI</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">b.</span>
                    {!! Form::text('source_coax_b', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('source_coax_b','<p class="help-block">:message</p>') !!}
                    <span class="block-span">PSI</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">c.</span>
                    {!! Form::text('source_coax_c', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('source_coax_c','<p class="help-block">:message</p>') !!}
                    <span class="block-span">PSI</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">d.</span>
                    {!! Form::text('source_coax_d', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('source_coax_d','<p class="help-block">:message</p>') !!}
                    <span class="block-span">GPM</span>                    </p>
            </div>
            <div class="col-md-6 col-sm-6 border-left clearfix">
                <p class="height-auto">LOAD COAX (WATER TO WATER APPLICATION)</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">a.</span> {!! Form::text('load_coax_a', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('load_coax_a','<p class="help-block">:message</p>') !!}<span class="block-span">PSI</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">b.</span> {!! Form::text('load_coax_b', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('load_coax_b','<p class="help-block">:message</p>') !!}<span class="block-span">PSI</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">c.</span> {!! Form::text('load_coax_c', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('load_coax_c','<p class="help-block">:message</p>') !!}<span class="block-span">PSI</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">d.</span> {!! Form::text('load_coax_d', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('load_coax_d','<p class="help-block">:message</p>') !!}<span class="block-span">GPM</span>                    </p>
            </div>

        </div>
        <!-- End of box-div -->

        <div class="box-1 box-2 clearfix">

            <div class="col-md-12 col-sm-12">
                <h3 class="h3-title">3. TEMPERATURE RISE / DROP ACROSS COAXIAL HEAT EXCHANGER*</h3>
            </div>

            <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto"></p>
                <p class="height-auto">WATER IN Temperature:</p>
                <p class="height-auto">WATER OUT Temperature:</p>
                <p class="height-auto">Temperature Difference:</p>

            </div>
           <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto underline clearfix">COOLING</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">e.</span> {!! Form::text('temperature_rise_drop_across_coaxial_heat_exchanger_cooling_e',
                    null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_coaxial_heat_exchanger_cooling_e','
                    <p
                        class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span> </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">f.</span> {!! Form::text('temperature_rise_drop_across_coaxial_heat_exchanger_cooling_f',
                    null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_coaxial_heat_exchanger_cooling_f','
                    <p
                        class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span> </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">g.</span> {!! Form::text('temperature_rise_drop_across_coaxial_heat_exchanger_cooling_g',
                    null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_coaxial_heat_exchanger_cooling_g','
                    <p
                        class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span> </p>

            </div>
            <div class="col-md-2 col-sm-4 border-left clearfix">
                <p class="height-auto underline clearfix">HEATING</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">e.</span> {!! Form::text('temperature_rise_drop_across_coaxial_heat_exchanger_heating_e', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_coaxial_heat_exchanger_heating_e','<p class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">f.</span> {!! Form::text('temperature_rise_drop_across_coaxial_heat_exchanger_heating_f', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_coaxial_heat_exchanger_heating_f','<p class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">g.</span> {!! Form::text('temperature_rise_drop_across_coaxial_heat_exchanger_heating_g', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_coaxial_heat_exchanger_heating_g','<p class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span>                    </p>
            </div>

            <div class="col-md-4 col-sm-12 clearfix">
                <div class="border-1">
                    <p>* Steps 3 - 9 should be conducted with the desuperheater disconnected.</p>
                </div>
            </div>

        </div>
        <!-- End of box-div -->

        <div class="box-1 box-2 clearfix">

            <div class="col-md-12 col-sm-12">
                <h3 class="h3-title">4. TEMPERATURE RISE / DROP ACROSS AIR COIL</h3>
            </div>

            <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto"></p>
                <p class="height-auto">SUPPY AIR Temperature:</p>
                <p class="height-auto">RETURN AIR Temperature:</p>
                <p class="height-auto">Temperature Difference:</p>

            </div>
            <div class="col-md-2 col-sm-4 clearfix">
                <p class="height-auto underline clearfix">COOLING</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">h.</span> {!! Form::text('temperature_rise_drop_across_air_coil_cooling_h', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_air_coil_cooling_h','<p class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">i.</span> {!! Form::text('temperature_rise_drop_across_air_coil_cooling_i', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_air_coil_cooling_i','<p class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">j.</span> {!! Form::text('temperature_rise_drop_across_air_coil_cooling_j', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_air_coil_cooling_j','<p class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span>                    </p>

            </div>
            <div class="col-md-2 col-sm-4 border-right clearfix">
                <p class="height-auto underline clearfix">HEATING</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">h.</span> {!! Form::text('temperature_rise_drop_across_air_coil_heating_h', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_air_coil_heating_h','<p class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">i.</span> {!! Form::text('temperature_rise_drop_across_air_coil_heating_i', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_air_coil_heating_i','<p class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">j.</span> {!! Form::text('temperature_rise_drop_across_air_coil_heating_j', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('temperature_rise_drop_across_air_coil_heating_j','<p class="help-block">:message</p>') !!}<span class="block-span text-capital"><sup>o</sup>F</span>                    </p>
            </div>

            <div class="col-md-5 col-sm-12 clearfix">
                <h3 class="h3-title text-right top-1 clearfix">LOAD COAX (WATER TO WATER APPLICATION)</h3>

                <div class="col-md-2 col-sm-2 clearfix">
                    <p class="height-auto clearfix"></p>
                    <p class="height-auto clearfix">LWT:</p>
                    <p class="height-auto clearfix">EWT:</p>
                    <p class="height-auto clearfix"></p>

                </div>
                <div class="col-md-5 col-sm-5 clearfix">
                    <p class="height-auto underline clearfix">COOLING</p>
                    <p class="height-auto clearfix"><span class="block-span w-30p">h.</span> {!! Form::text('load_coax_cooling_h', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('load_coax_cooling_h','<p class="help-block">:message</p>') !!}
                        <span class="block-span text-capital"><sup>o</sup>F</span>
                    </p>
                    <p class="height-auto clearfix"><span class="block-span w-30p">i.</span> {!! Form::text('load_coax_cooling_i', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('load_coax_cooling_i','<p class="help-block">:message</p>') !!}
                        <span class="block-span text-capital"><sup>o</sup>F</span>
                    </p>
                    <p class="height-auto clearfix"><span class="block-span w-30p">j.</span> {!! Form::text('load_coax_cooling_j', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('load_coax_cooling_j','<p class="help-block">:message</p>') !!}
                        <span class="block-span text-capital"><sup>o</sup>F</span>
                    </p>
                </div>
                <div class="col-md-5 col-sm-5 clearfix">
                    <p class="height-auto underline clearfix">HEATING</p>
                    <p class="height-auto clearfix"><span class="block-span w-30p">h.</span> {!! Form::text('load_coax_heating_h', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('load_coax_heating_h','<p class="help-block">:message</p>') !!}
                        <span class="block-span text-capital"><sup>o</sup>F</span>
                    </p>
                    <p class="height-auto clearfix"><span class="block-span w-30p">i.</span> {!! Form::text('load_coax_heating_i', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('load_coax_heating_i','<p class="help-block">:message</p>') !!}
                        <span class="block-span text-capital"><sup>o</sup>F</span>
                    </p>
                    <p class="height-auto clearfix"><span class="block-span w-30p">j.</span> {!! Form::text('load_coax_heating_j', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('load_coax_heating_j','<p class="help-block">:message</p>') !!}
                        <span class="block-span text-capital"><sup>o</sup>F</span>
                    </p>
                </div>
            </div>
        </div>

        <div class="box-1 box-2 clearfix">

            <div class="col-md-12 col-sm-12 clearfix">
                <h3 class="h3-title">5. HEAT OF REJECTION (HR) / HEAT OF EXTRACTION (HE) </h3>
            </div>
            <div class="clearfix">
                <div class="col-md-3 col-sm-3 clearfix">
                    <p class="height-auto">Brine Factor*:</p>
                </div>
                <div class="col-md-3 col-sm-3 clearfix">
                    <p class="height-auto clearfix"><span class="block-span w-30p">k.</span> {!! Form::text('HR_HE_brine_factor_k', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('HR_HE_brine_factor_k','<p class="help-block">:message</p>') !!} </p>
                </div>

                <div class="col-md-6 col-sm-12 clearfix">
                    <div class="border-1">
                        <p>* Use 500 for pure water, 485 for methanol, or 490 for calcium chloride. (This constant is derived
                            by multiplying the weight of one gallon of water (8.34) times the minutes in one hour (60) times
                            the specific heat of the fluid.
                        </p>
                        <p>
                            Water has a specific heat of 1.0
                        </p>
                    </div>
                </div>
            </div>

            <div class="clearfix">
                <div class="col-md-3 col-sm-4 clearfix">
                    <p class="height-auto"></p>
                    <p class="height-auto">HR/ HE = d x g x k:</p>
                </div>
                <div class="col-md-3 col-sm-4 clearfix">
                    <p class="height-auto underline clearfix">COOLING (HR)</p>
                    <p class="height-auto clearfix"><span class="block-span w-30p">i.</span> {!! Form::text('HR_cooling_i', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('HR_cooling_i','<p class="help-block">:message</p>') !!}
                        <span class="block-span text-capital">BTU / HR</span>
                    </p>
                </div>
                <div class="col-md-6 col-sm-4 clearfix">
                    <p class="height-auto underline clearfix">HEATING (HE)</p>
                    <p class="height-auto clearfix"><span class="block-span w-30p">i.</span> {!! Form::text('HE_heating_i', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('HE_heating_i','<p class="help-block">:message</p>') !!}
                        <span class="block-span text-capital">BTU / HR</span>
                    </p>
                </div>
            </div>

            <div class="clearfix">
                <p class="col-md-12 col-sm-12 m-20">*STEPS 6 - 9 NEED ONLY BE COMPLETED IF A PROBLEM IS SUSPECTED*</p>
            </div>
            <div class=" clearfix">

            </div>

        </div>
        <!-- End of box-div -->

        <div class="box-1 box-2 clearfix">

            <div class="col-md-12 col-sm-12">
                <h3 class="h3-title">6. WATTS</h3>
            </div>

            <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto"></p>
                <p class="height-auto">Volts:</p>
                <p class="height-auto">Total Amps (Comp. + Fan)*:</p>
                <p class="height-auto">Watts = m x n x 0.85</p>

            </div>
            <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto underline clearfix">COOLING</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">m.</span> {!! Form::text('watts_cooling_m', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('watts_cooling_m','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">VOLTS</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">n.</span> {!! Form::text('watts_cooling_n', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('watts_cooling_n','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">AMPS</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">o.</span> {!! Form::text('watts_cooling_o', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('watts_cooling_o','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">WATTS</span>                    </p>

            </div>
            <div class="col-md-3 col-sm-4 border-left clearfix">
                <p class="height-auto underline clearfix">HEATING</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">m.</span> {!! Form::text('watts_heating_m', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('watts_heating_m','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">VOLTS</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">n.</span> {!! Form::text('watts_heating_n', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('watts_heating_n','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">AMPS</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">o.</span> {!! Form::text('watts_heating_o', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('watts_heating_o','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">WATTS</span>                    </p>
            </div>

            <div class="col-md-3 col-sm-12 clearfix">
                <div class="border-1">
                    <p>* If there is only one source of power for the compressor and fan, Amp draw can be measured at the source
                        wiring connection.</p>
                </div>
            </div>

        </div>
        <!-- End of box-div -->

        <div class="box-1 box-2 clearfix">

            <div class="col-md-12 col-sm-12">
                <h3 class="h3-title">7. CAPACITY</h3>
            </div>

            <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto"></p>
                <p class="height-auto">Cooling Capacity = 1 - (0 x 3.413) </p>
                <p class="height-auto">Heating Capacity = 1 + (0 x 3.413)</p>
            </div>
            <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto underline clearfix">COOLING</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">p.</span> {!! Form::text('capacity_cooling_p', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('capacity_cooling_p','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">BTU / HR</span>                    </p>
                <p class="height-auto"></p>
            </div>
            <div class="col-md-6 col-sm-4 border-left clearfix">
                <p class="height-auto underline clearfix">HEATING</p>
                <p class="height-auto"></p>
                <p class="height-auto clearfix"><span class="block-span w-30p">p.</span> {!! Form::text('capacity_heating_p', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('capacity_heating_p','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">BTU / HR</span>                    </p>
            </div>

        </div>
        <!-- End of box-div -->

        <div class="box-1 box-2 clearfix">

            <div class="col-md-12 col-sm-12">
                <h3 class="h3-title">8. EFFICIENCY</h3>
            </div>

            <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto"></p>
                <p class="height-auto">Cooling EER = p / o </p>
                <p class="height-auto">Heating COP=.p / ( o x 3.413 )</p>
            </div>
            <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto underline clearfix">COOLING</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">q.</span> {!! Form::text('efficiency_cooling_q', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('efficiency_cooling_q','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">BTU / W</span>                    </p>
                <p class="height-auto"></p>
            </div>
            <div class="col-md-6 col-sm-4 border-left clearfix">
                <p class="height-auto underline clearfix">HEATING</p>
                <p class="height-auto"></p>
                <p class="height-auto clearfix"><span class="block-span w-30p">q.</span> {!! Form::text('efficiency_heating_q', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('efficiency_heating_q','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">BTU / BTU</span>                    </p>
            </div>

        </div>
        <!-- End of box-div -->

        <div class="box-1 box-2 clearfix">

            <div class="col-md-12 col-sm-12">
                <h3 class="h3-title">9. SUPERHEAT (S.H.) / SUBCOOLING (S.C)</h3>
            </div>

            <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto"></p>
                <p class="height-auto">Suction Pressure:</p>
                <p class="height-auto">Suction Saturation Temperature: </p>
                <p class="height-auto">Suction Line Temperature: </p>
                <p class="height-auto">S.H. = t-s </p>
                <p class="height-auto">Head Pressure: </p>
                <p class="height-auto">High Pressure Saturation Temperature: </p>
                <p class="height-auto">Liquid I ine Tomnaratur* </p>

            </div>
            <div class="col-md-3 col-sm-4 clearfix">
                <p class="height-auto underline clearfix">COOLING</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">r.</span> {!! Form::text('SH_SC_cooling_r', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_cooling_r','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">PSI</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">s.</span> {!! Form::text('SH_SC_cooling_s', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_cooling_s','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">DEG.F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">t.</span> {!! Form::text('SH_SC_cooling_t', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_cooling_t','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">DEG.F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">u.</span> {!! Form::text('SH_SC_cooling_u', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_cooling_u','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">DEG.F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">v.</span> {!! Form::text('SH_SC_cooling_v', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_cooling_v','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">PSI</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">w.</span> {!! Form::text('SH_SC_cooling_w', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_cooling_w','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">DEG.F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">x.</span> {!! Form::text('SH_SC_cooling_x', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_cooling_x','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">DEG.F</span>                    </p>
            </div>
            <div class="col-md-3 col-sm-4 border-left clearfix">
                <p class="height-auto underline clearfix">HEATING</p>
                <p class="height-auto clearfix"><span class="block-span w-30p">r.</span> {!! Form::text('SH_SC_heating_r', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_heating_r','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">PSI</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">s.</span> {!! Form::text('SH_SC_heating_s', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_heating_s','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">DEG.F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">t.</span> {!! Form::text('SH_SC_heating_t', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_heating_t','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">DEG.F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">u.</span> {!! Form::text('SH_SC_heating_u', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_heating_u','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">DEG.F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">v.</span> {!! Form::text('SH_SC_heating_v', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_heating_v','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">PSI</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">w.</span> {!! Form::text('SH_SC_heating_w', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_heating_w','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">DEG.F</span>                    </p>
                <p class="height-auto clearfix"><span class="block-span w-30p">x.</span> {!! Form::text('SH_SC_heating_x', null, ['class' => 'form-control input-100']) !!} {!! $errors->first('SH_SC_heating_x','<p class="help-block">:message</p>') !!}<span class="block-span text-capital">DEG.F</span>                    </p>
            </div>

            <div class="col-md-3 col-sm-12 clearfix">
                <div class="border-1">
                    <p>* Liquid line is between the coax and the expansion device in the cooling mode, between the air coil
                        and the expansion device in the heating mode.</p>
                </div>
            </div>



        </div>
        <!-- End of box-div -->
        <div class="box-1 box-2 clearfix">
            <div class="mrg-top pull-left">
                {!! Form::submit((isset($jobGeothermalForm) & $jobGeothermalForm != null)? 'Update' : 'Add', ['class' => 'btn btn-primary'])
                !!}
            </div>
        </div>

    </div>
</div>

@push('script-head')
<script type="text/javascript">
    $(function () {
        $('#install_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    })

</script>

@endpush