<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobLogs extends Model
{
    protected $table = 'job_logs';
    protected $fillable = [
        'job_id', 'user_id','start_datetime','end_datetime'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
