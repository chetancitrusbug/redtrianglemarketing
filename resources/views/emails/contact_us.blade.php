<!DOCTYPE html>
<html>
<head>
	<title>Contact Us - MJM</title>
</head>
<body>
    <p>Hello {{env('ADMIN_USER')}},</p>
    <p>An inquiry is generated. Please find below detail.</p>
	<table class="table table-hover">
		<tbody>
		  	<tr>
			    <td>Name :</td>
			    <td>{{$data->name}}</td>
		  	</tr>
		  	<tr>
			    <td>Phone :</td>
			    <td>{{$data->phone}}</td>
		  	</tr>
		  	<tr>
			    <td>Email :</td>
			    <td>{{$data->email}}</td>
		  	</tr>
		  	<tr>
			    <td>Message :</td>
			    <td>{{$data->message}}</td>
		  	</tr>
		</tbody>
  	</table>
</body>
</html>