<?php

return [

    'add_jobcode' => 'Add Job Code',
    'add_new_jobcode' => 'Add New Job Code',
    'back' => 'Back',
    'edit_jobcode' => 'Edit Job Code',
    'update' => 'Update',
    'job_code' => 'Job Code',
    'description' => 'Description',
    'jobcodes' => 'Job Codes',
    'id' => 'Id',
    'status' => 'Status',
    'actions' => 'Actions',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'blocked' => 'Blocked',
    'view' => 'View',
    'create' => 'Create',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'view_jobcode' => 'View Job Code',
    'jobcode' => 'Job Code',
];
