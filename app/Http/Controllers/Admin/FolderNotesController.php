<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Foldernotes;

class FolderNotesController extends Controller
{
    public function add(Request $request)
    {
       if(!empty($request->input('notes'))){
                $user_id=\Auth::user()->id;
		        $foldernotes = new Foldernotes();
                $foldernotes->job_id = $request->input('job_id');
                $foldernotes->notes = $request->input('notes');
                $foldernotes->folder_id = $request->input('note_folder_id');
                $foldernotes->user_type = 'admin';
                $foldernotes->employee_id = $user_id;
                $foldernotes->save();
        }
       return json_encode(array('msg'=>'Success'));

       exit;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

        $foldernotes = Foldernotes::find($id);
        $foldernotes->delete();

          $message='Deleted';
        return response()->json(['message'=>$message],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $data = Foldernotes::where('id',$id)->FirstOrFail();
        if($data){
            $code= 200;
        }else{
            $code = 400;
        }
        return response()->json(['data' => $data],$code);

    }

    public function notesupdate($id, Request $request)
    {

        $foldernotes = Foldernotes::findOrFail($id);
        $foldernotes->job_id = $request->input('job_id');
        $foldernotes->notes =$request->input('notes');
        $foldernotes->update();
        return json_encode(array('msg'=>'Success'));
         exit;
    }
}
