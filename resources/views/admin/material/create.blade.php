@extends('layouts.backend')


@section('title',trans('material.add_material'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('material.add_new_material')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('material.add_new_material')</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/material') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('material.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/material', 'class' => 'form-horizontal','id'=>'formmaterial','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.material.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection

