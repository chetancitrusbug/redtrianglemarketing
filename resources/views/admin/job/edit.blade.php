@extends('layouts.backend')

@section('title',trans('Jobs'))


@section('content')

<ul class="nav nav-tabs">
    <li class="jobadds"><a data-toggle="tab" href="#job">@lang('job.job_details')</a></li>
    {{-- <li class="jobtaskadds"><a data-toggle="tab" href="#task">@lang('job.task')</a></li> --}}
    <li class="jobformsadds"><a data-toggle="tab" href="#forms">@lang('job.forms')</a></li>
    <li class="jobfolderadds"><a data-toggle="tab" href="#folder">@lang('job.folder')</a></li>
    {{-- <li class="jobnotesadds"><a data-toggle="tab" href="#notes">@lang('job.notes')</a></li> --}}

</ul>
<div class="tab-content">
    <div id="job" class="tab-pane fade">
        <a href="{{ url('/admin/job') }}" title="Back">
            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Back To Listing </button>
        </a>
        @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif
        <ul class="nav nav-tabs">
            <li class="job_details_edit active in"><a data-toggle="tab" href="#jobdetails">@lang('job.job_detail')</a></li>
            <li class="order_form_list"><a data-toggle="tab" href="#order_form">@lang('job.jobcard_list')</a></li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div id="jobdetails" class="col-md-12 tab-pane active" role="tabpanel" aria-labelledby="makeRequest-tab-job">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('job.edit_job')</div>
                    <div class="panel-body">


                        {!! Form::model($job, [
                            'method' => 'PATCH',
                            'url' => ['/admin/job', $job->id],
                            'class' => 'form-horizontal',
                            'id'=>'formJob',
                            'enctype'=>'multipart/form-data'
                        ]) !!}

                        @include ('admin.job.form', ['submitButtonText' => trans('job.update')])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>

            <div id="order_form" class='fade tab-pane' role="tabpanel" aria-labelledby="makeRequest-tab-order" >
                <div><h2>Order List</h2></div>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Total</th>
                                <th>Employee Name</th>
                                <th>Actions</th>
                            </tr>
                            @if(count($jobcarddetail)>0)
                                @foreach($jobcarddetail as $jobcarddetails)

                                    <tr>
                                        <td>{{$jobcarddetails->id}}</td>
                                        <td>{{$jobcarddetails->job_title}}</td>
                                        <td>{{$jobcarddetails->total_amount}}</td>
                                        <td>{{(isset($jobcarddetails->jobcarduserName->name) ? $jobcarddetails->jobcarduserName->name : '')}}</td>
                                         <td><a href="{{url('admin/jobcard/'.$jobcarddetails->id)}}"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('job.view')</button></a></td>
                                    </tr>
                                @endforeach
                            @else
                                 <tr>
                                         <td></td>
                                         <td></td>
                                         <td colspan="2">No Orders Found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>

    {{-- <div id="notes" class="tab-pane fade">
        <a href="{{ url('/admin/job') }}" title="Back">
			<button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Back To Listing</button>
		</a>
		<a href="#" class="btn btn-warning btn-sm addnotesfolder" title="Add New Notes" id="addnotesfolder">
            <i class="fa fa-plus" aria-hidden="true"></i>@lang('job.add_notes')
        </a>
		<br><br>

         <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade active in" id="jobtaskdetails" role="tabpanel" aria-labelledby="tab-jobdetails">

                <div><h2>Notes List</h2></div>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                             <tr>
                               <!-- <th>Id</th>-->
                                <th>Notes</th>
                                <th>Date</th>
                                <th>Employee Name</th>
                                <th>Actions</th>
                            </tr>
                             @if(count($notes))
                                @foreach($notes as $notesdetail)
                                <tr>
                                    <td>{{$notesdetail->notes}}</td>
                                    <td>{{$notesdetail->updated_at->format('m-d-Y') }} ({{$notesdetail->updated_at->format('h:i:s a') }})</td>
                                    @if($notesdetail->user_type=='employee')
                                        <td>{{(isset($notesdetail->noteslist->name) ? $notesdetail->noteslist->name : '')}}</td>
                                    @else
                                        <td>{{$notesdetail->user_type}}</td>
                                    @endif
                                     <td><a href='#' value="{{$notesdetail->id}}" data-id="{{$notesdetail->id}}" class='edit-notesitem' ><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>
                                    <a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-notesitem' data-id="{{$notesdetail->id}}"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a></td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4"  class="nofound_align">No Notes</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

            </div>


        </div>

    </div> --}}
    <div id="folder" class="tab-pane fade">
        <a href="{{ url('/admin/job') }}" title="Back">
			<button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Back To Listing</button>
		</a>
		<a href="#" class="btn btn-warning btn-sm" title="Add New Folder" id="addjobfolder">
            <i class="fa fa-plus" aria-hidden="true"></i>Add Folder
        </a>
		<br><br>
           @if(count($folder))
                <ul class="nav nav-tabs subfolderul">
                    <?php $cls=''; ?>
                    @foreach($folder as $folders)
                    @if($folders->folder_name=='Electrical')
                       <?php  $cls='electricli'; ?>
                    @endif
                    @if($folders->folder_name=='Heating')
                       <?php  $cls='heatingli'; ?>
                    @endif
                    @if($folders->folder_name=='Cooling')
                       <?php  $cls='coolingli'; ?>
                    @endif
                    @if($folders->folder_name=='Plumbing')
                       <?php  $cls='plumbingli'; ?>
                    @endif
                    @if($folders->folder_name=='Other')
                       <?php  $cls='otherli'; ?>
                    @endif
                        <li class="sub_folder_id_{{$folders->id}} subfol_lists"><a data-toggle="tab" href="#jobdetails{{$folders->id}}" class ="<?php echo $cls; ?>">  {{$folders->folder_name}}</a></li>
                    @endforeach
                </ul>
           @endif
        <div class="tab-content" id="myTabContent">
         @if(count($folder))
            @foreach($folder as $folders)
            <div class="tab-pane subfolderdiv fade" id="jobdetails{{$folders->id}}" role="tabpanel" aria-labelledby="tab-jobdetails{{$folders->id}}">
                <div class="row">
                    <div class="col-md-12" >
                        <div class="box bordered-box blue-border">
                            <div class="box-content ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="#" class="btn btn-success btn-sm addfolderdocumentfolder" title="Add New Document" data-folderid = '{{$folders->id}}' id="ddd" >
                                            <i class="fa fa-plus" aria-hidden="true"></i>Add Folder Document
                                        </a>
                                        <a href="#" class="btn btn-success btn-sm addvideofolder" title="Add New Video" data-folderid = '{{$folders->id}}' id="vidid" >
                                            <i class="fa fa-plus" aria-hidden="true"></i>Add Video Document
                                        </a>
                                        <a href="#" class="btn btn-success btn-sm addnotesfolder" title="Add New Note" data-folderid='{{$folders->id}}' id="noteid">
                                            <i class="fa fa-plus" aria-hidden="true"></i>Add Note
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div><h2>Document List</h2></div>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>

                            <tr>
                                <!--<th>Id</th>-->
                                <th>Name</th>
                                <th>Employee Name</th>
                                <th>Document</th>
                                <th>Actions</th>
                            </tr>
                            @if(count($folders->documentList))
                            @foreach($folders->documentList as $docdetail)

                                <tr>
                                    <!--<td>{{$docdetail->f_id}}</td>-->
                                    <td>{{$docdetail->doc_title}}</td>
                                     @if($docdetail->user_type=='employee')
                                    <td>{{$docdetail->user_name}}</td>
                                     @else
                                    <td>{{$docdetail->user_type}}</td>
                                     @endif
                                      <td><a href="{{ url('Document') }}/{{ $docdetail->document_url }}" target="_blank">Download here<a></td></a>
                                    <td><a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-itemdoc' data-id="{{$docdetail->f_id}}"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a></td>
                                </tr>
                            @endforeach
                        @else
                                 <tr>
                                        <td colspan="3"  class="nofound_align">No Documents</td>
                                </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div><h2>Video List</h2></div>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>

                            <tr>
                               <!-- <th>Id</th>-->
                                <th>Name</th>
                                <th>Employee Name</th>
                                <th>Video</th>
                                <th>Actions</th>
                            </tr>
                             @if(count($folders->videoList))
                                @foreach($folders->videoList as $videodetail)

                                    <tr>
                                        <!--<td>{{$videodetail->v_id}}</td>-->
                                        <td>{{$videodetail->vid_title}}</td>
                                        @if($videodetail->user_type=='employee')
                                    <td>{{$videodetail->user_name}}</td>
                                     @else
                                    <td>{{$videodetail->user_type}}</td>
                                     @endif
                                      <td><a href="{{ url('Video') }}/{{ $videodetail->video_url }}" target="_blank">Download here<a></td></a>
                                        <td><a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="{{$videodetail->v_id}}"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a></td>

                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3"  class="nofound_align">No Videos</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

                <div>
                    <h2>Notes List</h2>
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <!-- <th>Id</th>-->
                                <th>Notes</th>
                                <th>Date</th>
                                <th>Employee Name</th>
                                <th>Actions</th>
                            </tr>
                            @if(count($notes)) @foreach($notes as $notesdetail)
                            @if($notesdetail->folder_id == $folders->id)
                            <tr>
                                <td>{{$notesdetail->notes}}</td>
                                <td>{{$notesdetail->updated_at->format('m-d-Y') }} ({{$notesdetail->updated_at->format('h:i:s a') }})</td>
                                @if($notesdetail->user_type=='employee')
                                <td>{{(isset($notesdetail->noteslist->name) ? $notesdetail->noteslist->name : '')}}</td>
                                @else
                                <td>{{$notesdetail->user_type}}</td>
                                @endif
                                <td><a href='#' value="{{$notesdetail->id}}" data-id="{{$notesdetail->id}}" class='edit-notesitem'><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>
                                    <a href='javascript:void(0);' class='btn btn-primary btn-xs'><button class='btn btn-danger btn-xs del-notesitem' data-id="{{$notesdetail->id}}"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a></td>
                            </tr>
                            @endif
                            @endforeach @else
                            <tr>
                                <td colspan="4" class="nofound_align">No Notes</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

            </div>
            @endforeach
            @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="box bordered-box blue-border">
                            <div class="box-header blue-background">
                                <div class="title">
                                    Oops! No Folders Found
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>

    </div>
    <div id="forms" class="tab-pane fade">
        <a href="{{ url('/admin/job') }}" title="Back">
        			<button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Back To Listing</button>
        		</a> {{-- <a href="#" class="btn btn-warning btn-sm addformsfolder" title="Add New Forms" id="addformsfolder">
                    <i class="fa fa-plus" aria-hidden="true"></i>@lang('job.add_forms')
                </a> --}}
        <br><br>
        <div class="tab-content" id="myTabContent">
            <div>
                <h2>Forms </h2>
            </div>
            <div class="table-responsive">
                <table class="table table-borderless jobGeothermalForm-table">
                    <tbody>
                        <tr>
                            <!-- <th>Id</th>-->
                            <th width="80%">Form Name</th>
                            <th width="20%">Actions</th>
                        </tr>

                        <tr>
                            <td>Geo Thermal Form</td>
                            @if($jobGeothermalForm != null)
                            <td><a href='#' value="{{$jobGeothermalForm->id}}"
                                    data-id="{{$jobGeothermalForm->id}}" class='addEditJobGeothermalForm'>
                                    <button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>                                @else
                                <td><a href='#' class='addEditJobGeothermalForm'><button class='btn btn-primary btn-xs'><i class='fa fa-plus' aria-hidden='true'></i>Add</button></a>                                    @endif
                        </tr>

                    </tbody>
                </table>
                <div class="col-md-12 form-section">
                    @if($jobGeothermalForm == null)
                    {!! Form::open(['url' => '/admin/jobGeothermalForm', 'class' => 'form-horizontal','id'=>'jobGeothermalForm','enctype'=>'multipart/form-data']) !!}
                    @else
                    {!! Form::model($jobGeothermalForm, [ 'method' => 'PATCH', 'url' => ['/admin/jobGeothermalForm', $jobGeothermalForm->id], 'class' => 'form-horizontal',
                    'id'=>'jobGeothermalForm', 'enctype'=>'multipart/form-data' ]) !!}
                    @endif
                    @include("admin.models.forms")
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
<div>
@include("admin.models.folder_add")
@include("admin.models.document_folder_add")
@include("admin.models.video_folder_add")
{{-- @include("admin.models.job_task_add") --}}
@include("admin.models.notes_folder_add")
@include("admin.models.taskfolder_add")

@endsection
@push('script-head')
<script>
$('.form-section').hide();
$('.jobGeothermalForm-table').show();

$(document).on('click', '.addEditJobGeothermalForm', function (e) {

        $('.form-section').show();
        $('.jobGeothermalForm-table').hide();

});
$(document).on('click', '.backToForm', function (e) {

        $('.form-section').hide();
        $('.jobGeothermalForm-table').show();

});

//for create and open New Job Folder
$(document).on('click', '#addjobfolder', function (e) {

         $('#folderAdd').modal('show');

});

$("#folder_add_form").validate({

    rules: {
         folder_name: {
                required: true
            },

    },

    submitHandler: function (form) {

        var method = "post"
        var c_id = '{{ $job->id}}';
        var folder_add_url= '{{url('/admin/folder')}}'+'/'+c_id;
       console.log(folder_add_url);
        $.ajax({
            type: method,
            url: folder_add_url,
            data: $(form).serialize(),
            beforeSend: function () {
            },

            success: function (data)
            {
                data = JSON.parse(data)
				var folder_id= data.data.id;
                if(data.msg == 'Success'){
                    toastr.success('Added Successfully',data.message);
                     /* Add Active class After Post Data*/
                var folderurlactive = window.location.href+folder_id+'?folderactive';// Returns full URL
                window.location.href=folderurlactive;
                location.reload();
                }else{
                    toastr.error('Folder is already exist',data.message)
                $('#folderAdd').modal('hide');
                }
            },
            error: function (error) {
                $('#folderAdd').modal('hide');
            }

        });
        return false;

    }
});

//for create and open New Job Folder
$(document).on('click', '#addjobtaskfolder', function (e) {

         $('#taskfolderAdd').modal('show');

});

$("#taskfolder_add_form").validate({

    rules: {
         folder_name: {
                required: true
            },

    },

    submitHandler: function (form) {

        var method = "post"
        var c_id = '{{ $job->id}}';
        var folder_add_url= '{{url('/admin/taskfolder')}}'+'/'+c_id;
        $.ajax({
            type: method,
            url: folder_add_url,
            data: $(form).serialize(),
            beforeSend: function () {
            },

            success: function (data)
            {

                data = JSON.parse(data)
				 var folder_id= data.data.id;
				// console.log(ids);
                if(data.msg == 'Success'){
                    toastr.success('Added Successfully',data.message);

                     /* Add Active class After Post Data*/
                var folderurlactive = window.location.href+folder_id+'?taskfolderactive';// Returns full URL
                window.location.href=folderurlactive;
                location.reload();
                }else{
                    toastr.error('Folder is already exist',data.message)
                $('#taskfolderAdd').modal('hide');
                }
            },
            error: function (error) {
                $('#taskfolderAdd').modal('hide');
            }

        });
        return false;

    }
});


//for create and open New Document Folder
$(document).on('click', '.addfolderdocumentfolder', function (e) {
    var folderdoc_id =$(this).attr('data-folderid');
    var folder_docadd_url= '{{url('/admin/folderdocument')}}';
    $('#folderdocumentAdd').modal('show');

        $.ajax({
            url: folder_docadd_url,
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {
            if(data.document){
                $("#document_id").empty();
                $("#document_id").append('<option disabled>Select Document</option>');
                $.each(data.document,function(key,value){
                    $("#document_id").append('<option value="'+value.id+'">'+value.title+'</option>');
                });

            }else{
               $("#document_id").empty();
            }
            $('#documentfolderid').val(folderdoc_id);
            }
        });
});

$("#folder_document_add_form").validate({

    rules: {
        document_id: {
            required: false,
        },

    },
    messages: {
        document_id: {

            required: "Please Select Document",
        },

    },
    submitHandler: function (form) {
		var folderdoc_id =$('#documentfolderid').val();
      //alert(folderdoc_id);
        var folder_docadd_url= '{{url('/admin/folderdocument')}}';
        $(".none").removeClass("error_message");
        var method = "post"
        var options = $('#document_id > option:selected');
                if(options.length == 0){
                   $('.document_error').html('<p>Please Select Document</p>');
                    return false;
                }
        $.ajax({
            type: method,
            url: folder_docadd_url,
            data: $('#folder_document_add_form').serialize(),
            beforeSend: function () {
            },


            success: function (data)
            {
                data = JSON.parse(data)
                if(data.msg == 'Success')
                    toastr.success('Added Successfully',data.message)
                else
                    toastr.error('Added Successfully',data.message)
                $('#folderdocumentAdd').modal('hide');
                /* Add Active class After Post Data*/
                var folderurlactive = window.location.href+folderdoc_id+'?folderactive';// Returns full URL
                window.location.href=folderurlactive;
                location.reload();
            },
            error: function (error) {
                $('#folderdocumentAdd').modal('hide');
               // toastr.error(data.message);
            }

        });
        return false;

    }
});
//for create and open New Video Folder
$(document).on('click', '.addvideofolder', function (e) {
    var foldervideo_id =$(this).attr('data-folderid');

    var folder_videoadd_url= '{{url('/admin/foldervideo')}}';

    $('#videofolderAdd').modal('show');

        $.ajax({
            url: folder_videoadd_url,
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {
            if(data.video){
                $("#video_id").empty();
                $("#video_id").append('<option disabled>Select Video</option>');
                $.each(data.video,function(key,value){
                    $("#video_id").append('<option value="'+value.id+'">'+value.title+'</option>');
                });

            }else{
               $("#video_id").empty();
            }
            $('#videofolderid').val(foldervideo_id);
            }

        });
});

$("#folder_video_add_form").validate({

    rules: {
        video_id: {
            required: false,
        },

    },
    messages: {
        video_id: {
            required: "Please Select Document",
        },

    },
    submitHandler: function (form) {
		var foldervideo_id =$(this).attr('data-folderid');
        var foldervideoid =$('#videofolderid').val();
        var folder_videoadd_url= '{{url('/admin/foldervideo')}}';
        var method = "post"
        var str=$('#folder_video_add_form').serialize();
        var options = $('#video_id > option:selected');
                if(options.length == 0){
                   $('.video_error').html('<p>Please Select Video</p>');
                    return false;
                }

        $.ajax({
            type: method,
            url: folder_videoadd_url,
            data: $('#folder_video_add_form').serialize(),
            beforeSend: function () {
            },

            success: function (data)
            {
                data = JSON.parse(data)

                if(data.msg == 'Success')
                    toastr.success('Added Successfully',data.message)
                else
                    toastr.error('Added Successfully',data.message)
                $('#videofolderAdd').modal('hide');
                 /* Add Active class After Post Data*/
                var folderurlactive = window.location.href+foldervideoid+'?folderactive';// Returns full URL
                window.location.href=folderurlactive;
                location.reload();
            },
            error: function (error) {
                $('#videofolderAdd').modal('hide');
            }

        });
        return false;

    }
});
//Delete Video  Folder by id
$(document).on('click', '.del-item', function (e) {

        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/foldervideo/') }}";
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Video ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    location.reload();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
});
//Delete Job Task  by id
$(document).on('click', '.del-itemjobtask', function (e) {

        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/jobtask/') }}";
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Task ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    location.reload();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
});
//Delete Document Folder by id
$(document).on('click', '.del-itemdoc', function (e) {

        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/folderdocument/') }}";
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Document ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    location.reload();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
});

//for create and open New Job Task Model
$(document).on('click', '#addjobtask', function (e) {
    var job_id = '{{ $job->id}}';
     var folderdoc_id =$(this).attr('data-taskfolderid');

     var jobtask_url= '{{url('/admin/jobtask')}}/add/'+job_id;

         $('#jobtaskAdd').modal('show');

          $.ajax({
            url: jobtask_url,
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {

            if(data.user){
                $("#employee_id").empty();
                $("#employee_id").append('<option disabled>Select Employee</option>');
                $.each(data.user,function(key,value){
                    $("#employee_id").append('<option value="'+value.id+'">'+value.name+'</option>');
                });

            }else{
               $("#employee_id").empty();
            }
            $('#taskfolderid').val(folderdoc_id);
            }

        });

});

$("#job_task_add_form").validate({

    rules: {
        title: {
            required: true,
        },
        description: {
            required: true,
        },

    },
    messages: {
        title: {
            required: "Please Enter Title",
        },
        description: {
            required: "Please Enter Description",
        },
    },
    submitHandler: function (form) {
       var folderdoc_id =$('#taskfolderid').val();
		var edit_id=$("#updjobtaskid").val();
        var jobs_id = '{{ $job->id}}';
        var jobtask_url= "{{url('/admin/jobtask')}}/add/"+jobs_id;
        var method = "post";
       if(edit_id!='' && edit_id!=0){
        var jobtask_url= "{{url('/admin/jobtask')}}/update/"+edit_id;
         var folderdoc_id =$('#updjobfoldertaskid').val();
        //var method = "put";
       }


        var str=$('#job_task_add_form').serialize();
         var options = $('#employee_id > option:selected');
                if(options.length == 0){
                   $('.employee_error').html('<p>Please Select Employee</p>');
                    return false;
                }
        $.ajax({
            type: method,
            url: jobtask_url,
            data: $('#job_task_add_form').serialize(),
            beforeSend: function () {
            },

            success: function (data)
            {
                data = JSON.parse(data)

                if(data.msg == 'Success')
                    toastr.success('Added Successfully',data.message)
                else
                    toastr.error('Added Successfully',data.message)
                $('#jobtaskAdd').modal('hide');
                    /* Add Active class After Post Data*/
                    var folderurlactive = window.location.href+folderdoc_id+'?taskfolderactive';// Returns full URL
                    window.location.href=folderurlactive;
                    location.reload();
            },
            error: function (error) {
                $('#jobtaskAdd').modal('hide');
            }

        });

        return false;

    }
});
$(document).on('click', '.edit-item', function (e) {
   // var folderdoc_id =$('#taskfolderid').val();
    var folderdoc_id = $(this).attr('data-taskfolderid');
    console.log(folderdoc_id);
    var id = $(this).attr('data-id');
    var burl = "{{url('admin/jobtask')}}/"+id+"/edit";
    // alert(burl);
    $.get(burl, function (result) {

             var data =result.data;
             var employees=result.employees;
             var emp_id=employees.employee_id;
             var users=result.user;

            $('#jobtasktitle').val(data.title);
            $('#jobtaskdescription').val(data.description);

           var employee  = []
            var employee_option = '';
            $.each(users,function(index,json){
                employee_option += '<option value="' +  json.id + '" >' + json.name + '</option>';
            });
            $('#employee_id').html(employee_option);
            $.each(employees,function(index,json){
                //console.log(json.employee_id);
                $("#employee_id option[value='" + json.employee_id + "']").prop("selected", true);

            });

                $("#updjobtaskid").val(id);
                 $("#updjobfoldertaskid").val(folderdoc_id);
                $('.jobtask-title').html('Update Job Task');

        })
         $('#jobtaskAdd').modal('show');


    });

 $(document).on('click', '.addnotesfolder', function (e) {
    var folder_id = $(this).attr('data-folderid');
    $('#note_folder_id').val(folder_id);
    $('#foldernotesAdd').modal('show');
});
$("#folder_notes_add_form").validate({

    rules: {
        notes: {
            required: true,
        },
    },
    submitHandler: function (form) {
        var edit_id=$("#upnotesid").val();
        var foldernotes_id =$('#notid').attr('data-folderid');
        var foldervideoid = $('#foldernotesAdd').val();
        var folder_notes_url= '{{url('/admin/foldernotes')}}';
        var method = "post";
        if(edit_id!='' && edit_id!=0){
            var folder_notes_url= "{{url('/admin/foldernotes')}}/update/"+edit_id;
        }
        var str=$('#folder_notes_add_form').serialize();

        $.ajax({
            type: method,
            url: folder_notes_url,
            data: $('#folder_notes_add_form').serialize(),
            beforeSend: function () {
            },

            success: function (data)
            {
                data = JSON.parse(data)

                if(data.msg == 'Success'){
                    toastr.success('Added Successfully',data.message)
                }
                else{
                    toastr.error('Added Successfully',data.message)
                }

                $('#foldernotesAdd').modal('hide');

                 var taskurlactive = window.location.href+'folderlist';// Returns full URL

                window.location.href=taskurlactive;
                location.reload();
            },
            error: function (error) {
                $('#foldernotesAdd').modal('hide');
            }

        });
        return false;

    }
});
$(document).on('click', '.edit-notesitem', function (e) {
        var id = $(this).attr('data-id');
         var burl = "{{url('admin/foldernotes')}}/"+id+"/edit";
            $.get(burl, function (result) {
                var data =result.data;
                $('#notess').val(data.notes);
                $("#upnotesid").val(id);
                $('.notes-title').html('Update Notes');
            })
         $('#foldernotesAdd').modal('show');
    });

//Delete Notes Folder by id
$(document).on('click', '.del-notesitem', function (e) {

        var id = $(this).attr('data-id');
		var url ="{{ url('/admin/foldernotes/') }}";
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Notes ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    location.reload();
                    toastr.success('Action Success!', data.message);
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Proceed!',erro)
                }
            });
        }
});
</script>
    @endpush
