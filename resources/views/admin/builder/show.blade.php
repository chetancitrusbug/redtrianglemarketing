@extends('layouts.backend')

@section('title',trans('builder.view_builder'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('builder.builder')</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('builder.back')
                        </button>
                    </a>
                    
                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('builder.id')</td>
                                <td>{{ $builder->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('builder.name')</td>
                                <td>{{ $builder->name }}</td>
                            </tr>
                            <tr>
                                <td>@lang('builder.address')</td>
                                <td>{{ $builder->address }}</td>
                            </tr>
                             <tr>
                                <td>@lang('builder.email')</td>
                                <td>{{ $builder->name }}</td>
                            </tr>
                            <tr>
                                <td>@lang('builder.phone')</td>
                                <td>{{ $builder->phone }}</td>
                            </tr>
                      
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection