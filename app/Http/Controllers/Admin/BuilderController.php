<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Builder;
use Session;
use Yajra\Datatables\Datatables;
use DB;
use App\AssignclientBuilder;
use App\AssignjobBuilder;
use App\Client;
use App\Job;

class BuilderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
       
        return view('admin.builder.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {  
        
        return view('admin.builder.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required', 
            'address'=>'required',
            'email'=>'required|unique:builder',
            'phone'=>'required|numeric|digits_between:10,11',       
        ]);
		$data = $request->all();

        $builder = Builder::create($data);

        Session::flash('flash_message', 'Builder added!');

        return redirect('admin/builder');
    }
    public function datatable(request $request)
    {
        $builder = Builder::select('*')->orderBy('id','desc')->where('id','!=','1');        

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(name LIKE  '%$value%')";

                $builder=Builder::whereRaw($where_filter);
            }
        }     
        return Datatables::of($builder)
            ->make(true);
        exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $builder = Builder::find($id);
        
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $builder->status= 'inactive';
                $builder->update();            

                return redirect()->back();
            }else{
                $builder->status= 'active';
                $builder->update();               
                return redirect()->back();
            }

        }
	/* Check builder is exist or not */
        if($builder){
            return view('admin.builder.show', compact('builder'));
        }
        else{
             return redirect('/admin/builder');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $request->id=$id;
		$builder = Builder::where('id',$id)->first();

        return view('admin.builder.edit', compact('builder'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'name' => 'required', 
            'address'=>'required',
            'phone'=>'required|numeric|digits_between:10,11',     
        ]);
        $requestData = $request->all();  

        $builder = Builder::findOrFail($id);

	    $builder->update($requestData);

        flash('Builder Updated Successfully!');
		
        return redirect('admin/builder');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        $builder = Builder::find($id);
        $assignclienttobuilder =AssignclientBuilder::where('client_builder_id',$id)->get();
        if(count($assignclienttobuilder) > 0){
        foreach($assignclienttobuilder as $delassignbuilder){
                $client=Client::find($delassignbuilder->client_id);
                if(count($client) > 0 ){               
                    $client->delete();
                }
                $delassignbuilder->delete();  
            }
        }
        $assignjobtobuilder =AssignjobBuilder::where('job_builder_id',$id)->get();
        if(count($assignjobtobuilder) > 0){
            foreach($assignjobtobuilder as $delassignjobbuilder){
                $job=Job::find($delassignjobbuilder->job_id);
                if(count($job) > 0){
                    $job->delete();
                }
                $delassignjobbuilder->delete();  
            }
        }
        $builder->delete();
        $message='Deleted';
        return response()->json(['message'=>$message],200);
    }  
}
