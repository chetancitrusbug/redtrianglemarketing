
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', trans('product.name'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('product.description'), ['class' => 'col-md-4 control-label required']) !!}
     <div class="col-md-6">
    {!! Form::textarea('description', null, ['class' => 'form-control','size' => '30x5']) !!}
    {!! $errors->first('description', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('category_id') ? ' has-error' : ''}}">
    {!! Form::label('category_id', trans('product.category'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        
         {!! Form::select('category_id',$category, null, ['class' => 'form-control']) !!}
          {!! $errors->first('category_id', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('unique_part_number') ? ' has-error' : ''}}">
    {!! Form::label('unique_part_number', trans('product.unique_part_number'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('unique_part_number', null, ['class' => 'form-control']) !!}
        {!! $errors->first('unique_part_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('vender_part_number') ? ' has-error' : ''}}">
    {!! Form::label('vender_part_number', trans('product.vender_part_number'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('vender_part_number', null, ['class' => 'form-control']) !!}
        {!! $errors->first('vender_part_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(isset($product) && $product->image)

@foreach($images as $img_name)
<img src="{!! $img_name->image !!}" alt="" width="100px">
@endforeach
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image',  'change image', ['class' => 'col-md-4 control-label required']) !!}
        <div class="col-md-6">
            <input type="file" name="image[]" id="image" multiple >
            {!! $errors->first('image', '<p class="help-block with-errors">:message</p>') !!}
        </div>
    </div>
@else
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image',  trans('product.image'), ['class' => 'col-md-4 control-label required']) !!}
        <div class="col-md-6">
            <input type="file" name="image[]" id="image" multiple >
            {!! $errors->first('image', '<p class="help-block with-errors">:message</p>') !!}
        </div>
    </div>
    
@endif
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', trans('product.status'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
         {!! Form::select('status',['' =>'Select Status', 'active'=>'Active','inactive'=>'Inactive'] ,null, ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('product.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

