<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\EmailController;
use App\Job;
use App\User;
use App\Jobassignemployee;
use App\Jobtask;
use App\Folder;
use App\Foldervideo;
use App\Folderdocument;
use App\Video;
use App\Document;
use App\Jobcarddetail;
use App\Jobcard;
use DB;


class JobsController extends Controller
{
    public function __construct()
    {
        $this->mail_function = new EmailController();
    }
    public function joblist(Request $request)
    {
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users = User::where('users.id', $id)->first();
        if ($users) {
            $joblistdata = Jobassignemployee::with('jobList')
                ->where('jobassignemployee.job_employee_id', $id)
                ->get();
            if (count($joblistdata) > 0) {
                foreach ($joblistdata as $Jobsdata) {

                    foreach ($Jobsdata->jobList as $joblistsdata) {
                        $data[] = $joblistsdata;
                        $message = 'Success';
                    }

                }

            } else {
                $status = false;
                $message = 'No Jobs Found';
                $code = 400;
            }
        } else {
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status' => $status, 'data' => $data, 'message' => $message, 'code' => $code]);
    }

    public function jobdetail(Request $request)
    {
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users = User::where('users.id', $id)->first();
        if ($users) {
            $jobdata = Job::with('clientName')
                ->where('id', $job_id)
                ->get();
            if (count($jobdata) > 0) {
                $data = $jobdata;
                $message = 'Success';
            } else {
                $status = false;
                $message = 'No Jobs Found';
                $code = 400;
            }
        } else {
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status' => $status, 'data' => $data, 'message' => $message, 'code' => $code]);
    }

    public function jobtasklist(Request $request)
    {
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users = User::where('users.id', $id)->first();
        if ($users) {
            $tasklistdata = Jobassignemployee::where('jobassignemployee.job_employee_id', $id)
                ->where('jobassignemployee.job_id', $job_id)
                ->with(['JobtaskList', 'checkemployeeJob' => function ($q) use ($id) {
                    $q
                        ->where('employeejob.employee_id', $id);
                }])

                ->get();


            if (count($tasklistdata) > 0) {
                $data = $tasklistdata;
                $message = 'Success';
            } else {
                $status = false;
                $message = 'No Job Task Found';
                $code = 400;
            }
        } else {
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status' => $status, 'data' => $data, 'message' => $message, 'code' => $code]);
    }

    public function jobfolderlist(Request $request)
    {
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users = User::where('users.id', $id)->first();
    }
}