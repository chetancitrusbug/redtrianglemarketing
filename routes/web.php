<?php

Auth::routes();


Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::get('/', 'Admin\AdminController@index');


    Route::group(['prefix' => 'admin'], function () {


        Route::get('/', 'Admin\AdminController@index');

        //generator
        Route::get('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
        Route::post('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);


        //Role Permission
        Route::resource('/permissions', 'Admin\PermissionsController');
        Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');

        //users
        Route::resource('/users', 'Admin\UsersController');
        Route::get('/users/create', 'Admin\UsersController@create');
        Route::get('/users/{id}/edit', 'Admin\UsersController@edit');
        Route::get('/usersdata', ['as' => 'UsersControllerUsersData', 'uses' => 'Admin\UsersController@datatable']);

        //users
        Route::resource('/sadmin', 'Admin\SadminController');
        Route::get('/sadmin/create', 'Admin\SadminController@create');
        Route::get('/sadmin/{id}/edit', 'Admin\SadminController@edit');
        Route::get('/sadmindata', ['as' => 'SadminControllerSadminData', 'uses' => 'Admin\SadminController@datatable']);

        //Builders
        Route::resource('/builder', 'Admin\BuilderController');
        Route::get('/builder/create', 'Admin\BuilderController@create');
        Route::get('/builder/{id}/edit', 'Admin\BuilderControllerBuilderData@edit');
        Route::get('/builderdata', ['as' => 'BuilderControllerBuilderData', 'uses' => 'Admin\BuilderController@datatable']);


        //Clients

        Route::get('/clients/downloadsamplecsv', 'Admin\ClientController@downloadsamplecsv');
        Route::get('/clients/createclient', 'Admin\ClientController@uploadclientcsv');
        Route::post('clients/createclient','Admin\ClientController@bulkuploadClientCsv');
        Route::resource('/clients', 'Admin\ClientController');
        Route::get('/clients/create', 'Admin\ClientController@create');

        Route::get('/clientsdata', ['as' => 'ClientControllerClientsData', 'uses' => 'Admin\ClientController@datatable']);
        Route::get('/api/get-state-list', ['as' => 'APIController@getStateList', 'uses' => 'Admin\APIController@getStateList']);
		Route::get('/api/get-city-list',['as' => 'APIControllergetCityList', 'uses' => 'Admin\APIController@getCityList']);
        //Videos
        Route::resource('/videos', 'Admin\VideoController');
        Route::get('/videos/create', 'Admin\VideoController@create');
        Route::get('/videosdata', ['as' => 'VideoControllerVideosData', 'uses' => 'Admin\VideoController@datatable']);
        //Route::delete('/video/{id}', 'VideoControllerVideosData@destroy')->name('video');

        //Document
        Route::resource('/documents', 'Admin\DocumentController');
        Route::get('/documents/create', 'Admin\DocumentController@create');
        Route::get('/documentsdata', ['as' => 'DocumentControllerDocumentsData', 'uses' => 'Admin\DocumentController@datatable']);

        //Category
        Route::resource('/category', 'Admin\CategoryController');
        Route::get('/category/create', 'Admin\CategoryController@create');
        Route::get('/categorydata', ['as' => 'CategoryControllerCategoryData', 'uses' => 'Admin\CategoryController@datatable']);

        //Product
        Route::resource('/products', 'Admin\ProductController');
        Route::get('/products/create', 'Admin\ProductController@create');
        Route::get('/productsdata', ['as' => 'ProductControllerProductsData', 'uses' => 'Admin\ProductController@datatable']);
        Route::post('products/checkInOutTools', 'Admin\ProductController@checkInOutTools');
        Route::get('/products/logs/{id}', 'Admin\ProductController@logsView');
        Route::get('/products/logs/datatable/{id}', 'Admin\ProductController@logsDatatable');


        //Job
        Route::resource('/job', 'Admin\JobController');
        Route::get('/job/create', 'Admin\JobController@create');
        Route::get('/jobsdata', ['as' => 'JobControllerJobsData', 'uses' => 'Admin\JobController@datatable']);

        //Archive Job
        Route::get('/archiveJob', 'Admin\JobController@archiveJob');
        Route::get('/archiveJobsData', ['as' => 'JobControllerArchiveJobsData', 'uses' => 'Admin\JobController@archiveJobDatatable']);

        //jobcode
        Route::resource('/jobcode', 'Admin\JobcodeController');
        Route::get('/jobcodedata', ['as' => 'JobcodeControllerJobcodeData', 'uses' => 'Admin\JobcodeController@datatable']);


        //roles
        Route::resource('/roles', 'Admin\RolesController');
        Route::get('/role', ['as' => 'RoleControllerRolesData', 'uses' => 'Admin\RolesController@datatable']);
       //Folder
        Route::get('/folder/{id}', 'Admin\FolderController@add');
        Route::post('/folder/{id}', 'Admin\FolderController@add');
         //Task Folder
        Route::get('/taskfolder/{id}', 'Admin\TaskfolderController@add');
        Route::post('/taskfolder/{id}', 'Admin\TaskfolderController@add');
        //FolderDocument
        Route::resource('/folderdocument', 'Admin\FolderDocumentController');
        Route::get('/folderdocument', 'Admin\FolderDocumentController@add');
        Route::post('/folderdocument', 'Admin\FolderDocumentController@add');

        //FolderNotes
        Route::resource('/foldernotes', 'Admin\FolderNotesController');
        Route::get('/foldernotes', 'Admin\FolderNotesController@add');
        Route::post('/foldernotes', 'Admin\FolderNotesController@add');
        Route::post('/foldernotes/update/{id}', 'Admin\FolderNotesController@notesupdate')->name('foldernotes');

        //Jobtask
       // Route::resource('/jobtask', 'Admin\JobtaskController');
        Route::get('/jobtask/add/{id}', 'Admin\JobtaskController@add');
        Route::post('/jobtask/add/{id}', 'Admin\JobtaskController@add');
        Route::get('/jobtask/{id}/edit', 'Admin\JobtaskController@edit');
        Route::delete('/jobtask/{id}', 'Admin\JobtaskController@destroy');
        Route::post('/jobtask/update/{id}', 'Admin\JobtaskController@jobtaskupdate')->name('jobtask');

       //Job Card
        Route::get('/jobcard/{id}', 'Admin\JobController@jobcardlist');

        //job forms

        Route::post('/jobGeothermalForm', 'Admin\JobController@addJobGeothermalForm');
        Route::patch('/jobGeothermalForm/{id}', 'Admin\JobController@editJobGeothermalForm');



        //Assign Job Dashboard
         Route::get('/assignjob', 'Admin\AdminController@assignJob');
        //filter with employee in calender view
         Route::get('/employeejobschedule', 'Admin\AdminController@employee_jobschedule');
        //FolderVideo
        Route::resource('/foldervideo', 'Admin\FolderVideoController');
        Route::get('/foldervideo', 'Admin\FolderVideoController@add');
        Route::post('/foldervideo', 'Admin\FolderVideoController@add');

	    //my profile
        Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
        Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
        Route::patch('/profile/edit', 'Admin\ProfileController@update');
        Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
        Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');


        //Setting
        Route::resource('/setting', 'Admin\SettingController');
        Route::get('/setting/create', 'Admin\SettingController@create');
        //Route::get('/builder/{id}/edit', 'Admin\BuilderControllerBuilderData@edit');
       // Route::get('/builderdata', ['as' => 'BuilderControllerBuilderData', 'uses' => 'Admin\BuilderController@datatable']);

       //Material
        Route::get('/material/datatable', 'Admin\MaterialController@datatable');
        Route::get('/material/logs/datatable/{id}', 'Admin\MaterialController@logsDatatable');
        Route::get('/material/logs/{id}', 'Admin\MaterialController@logsView');
        Route::resource('/material', 'Admin\MaterialController');


    });

});


Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
