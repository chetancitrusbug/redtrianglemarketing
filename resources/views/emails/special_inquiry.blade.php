<!DOCTYPE html>
<html>
<head>
	<title>Contact Us - MJM</title>
</head>
<body>
    <p>Hello {{env('ADMIN_USER')}},</p>
    <p>Special inquiry is generated. Please find below Attachments.</p>
	<table class="table table-hover">
		<tbody>
		  	<tr>
			    <td>File 1 :</td>
			    <td>{{$data->file_1}}</td>
		  	</tr>
		  	<tr>
			    <td>File 2 :</td>
			    <td>{{$data->file_2}}</td>
		  	</tr>
		  	<tr>
			    <td>File 3 :</td>
			    <td>{{$data->file_3}}</td>
		  	</tr>
		  	<tr>
			    <td>File 4 :</td>
			    <td>{{$data->file_4}}</td>
		  	</tr>
		  	<tr>
			    <td>Note :</td>
			    <td>{{$data->note}}</td>
              </tr>
            <tr>
                <td>Name :</td>
                <td>{{$name}}</td>
            </tr>
            <tr>
                <td>Email :</td>
                <td>{{$email}}</td>
            </tr>
		</tbody>
  	</table>
</body>
</html>