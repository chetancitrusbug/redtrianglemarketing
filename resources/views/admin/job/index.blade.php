@extends('layouts.backend')

@section('title',trans('job.jobs'))
@section('pageTitle',trans('job.jobs'))

@section('content')
	<div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('job.jobs')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ url('/admin/job/create') }}" class="btn btn-success btn-sm"
                                   title="Add New Job">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('job.add_new_job')
                                </a>

                        </div>

                        <div class="col-md-3">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/job', 'class' => 'form-horizontal', 'role' => 'search'])  !!}
                                <div class="form-group">
								<label for="filter_status" class="col-md-4 control-label ">Status</label>
								<div class="col-md-6">
                            {!! Form::select('status',array('active'=>'Active','inactive'=>'Inactive','all'=>'All'),'',['class'=>'form-control ', 'id'=>'filter_status']) !!}
								</div>
                            </div>
                            {!! Form::close() !!}
                            </div>

						<div class="col-md-3">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/job', 'class' => 'form-horizontal', 'role' => 'search'])  !!}
                                <div class="form-group">
								<label for="filter_job_status" class="col-md-4 control-label ">Job Status</label>
								<div class="col-md-6">
                            {!! Form::select('status',array('all'=>'All','markasdone'=>'Done','pending'=>'Pending',),'',['class'=>'form-control', 'id'=>'filter_job_status']) !!}
							</div>
                            </div>
                            {!! Form::close() !!}
                            </div>
                        </div>





                    <div class="table-responsive">
                        <table class="table table-borderless" id="jobs-table">
                            <thead>
                            <tr>
                               <!-- <th data-priority="1">@lang('job.id')</th> -->
                                <th data-priority="1">@lang('job.title')</th>
                                <th data-priority="2">@lang('job.description')</th>
								<th data-priority="4">@lang('job.jobStatus')</th>
                                <th data-priority="5">@lang('job.status')</th>
                                <th data-priority="3">@lang('job.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/job/') }}";

        datatable = $('#jobs-table').DataTable({
            //"order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('JobControllerJobsData') !!}',
                    type: "get", // method , by default get
                    data: function (d) {
                        d.filter_status = $('#filter_status').val();
                        d.filter_job_status = $('#filter_job_status').val();
                    }
                },
                columns: [
                   // { data: 'id', name: 'Id',"searchable": false },
                    { data: 'title',name:'title',"searchable" : false},
                    { data: 'description',name:'jobs.description',"searchable" : false},
					{
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                           if(o.job_status == 'markasdone')

								status = "Done";
                            else
								status = "Pending";

                            return status;

                        }
					},


                     {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                           if(o.status == 'inactive')

                             status = "<a href='"+url+"/"+o.id+"?status=inactive' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('job.inactive')</button></a>";
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('job.active')</button></a>";

                            return status;

                        }

                    },
					{
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";

                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";

                           // var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('job.view')</button></a>&nbsp;";


                            return e+d;
                        }
                    }

                ]
        });
    $('#filter_status').change(function() {
        datatable.draw();
    });
	$('#filter_job_status').change(function() {
        datatable.draw();
    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/job/') }}";
        url = url + "/" + id;
		//alert(url);
        var r = confirm("Are you sure you want to delete Job ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

</script>
@endpush
