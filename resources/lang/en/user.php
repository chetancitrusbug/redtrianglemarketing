<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
 */
    'add_user' => 'Add Employee',
    'add_new_user' => 'Add New Employee',
    'back' => 'Back',
    'edit_user' => 'Edit Employee',
    'update' => 'Update',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'role' => 'Role',
    'related_to' => 'Related To',
    'cpf' => 'CPF',
    'mobile' => 'Mobile',
    'phone' => 'Phone',
    'user_image' => 'Employee Image',
    'comments' => 'Comments',
    'create' => 'Create',
    'users' => 'Employees',
    'id' => 'ID',
    'profile' => 'Profile',
    'name' => 'Name',
    'email' => 'Email',
    'role' => 'Role',
    'company' => 'Company',
    'status' => 'Status',
    'actions' => 'Actions',
    'blocked' => 'Blocked',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'view' => 'View',
    'logs' => 'Logs',
    'view_user' => 'View Employee',
    'user' => 'Employee',
    'last_login_at' => 'Last Login At',
	'notes'=>'Notes',



    'label' => [
        'cbusers' => 'ConnectBahn Employee',
        'id' => 'Id',
        'profile' => 'Profile',
        'name' => 'Name',
        'email' => 'Email',
        'role' => 'Role',
        'company' => 'Company',
        'status' => 'Status',
        'actions' => 'Actions',
        'goal' => 'Goal',
        'cbusertransaction' => 'Last 30d Transactions Amount',
        'assigncustorsupp' => 'Assigned Customers/Suppliers',
        'changegoal' => 'Change Goal',
        'changegoaluser' => 'Change Goal For User',
        'active' => 'Active',
        'inactive' => 'Inactive',
        'blocked' => 'Blocked',
        'changeamount' => 'Change Goal Amount',
        'username' => 'User name'

        
    ],

    

];
