@extends('layouts.backend')

@section('title',trans('jobcode.jobcodes'))
@section('pageTitle',trans('jobcode.jobcodes'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('jobcode.jobcodes')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ url('/admin/jobcode/create') }}" class="btn btn-success btn-sm"
                                   title="Add New jobcode">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('jobcode.add_new_jobcode')
                                </a>

                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/jobcode', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}

                            {!! Form::close() !!}
                            </div>
                        </div>



                    <div class="table-responsive">
                        <table class="table table-borderless" id="jobcodes-table">
                            <thead>
                            <tr>
                               <!-- <th data-priority="1">@lang('jobcode.id')</th> -->
                                <th data-priority="3">@lang('jobcode.job_code')</th>
                                <th data-priority="7">@lang('jobcode.status')</th>
                                <th data-priority="8">@lang('jobcode.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/jobcode/') }}";

        datatable = $('#jobcodes-table').DataTable({
           // "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('JobcodeControllerJobcodeData') !!}',
                    type: "get", // method , by default get

                },
                columns: [
                   // { data: 'id', name: 'Id',"searchable": false },
                    { data: 'job_code',name:'job_code',"searchable" : true},
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status=='blocked')
                            status = "<a href='"+url+"/"+o.id+"?status=blocked' data-id="+o.id+" title='blocked'><button class='btn btn-default btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('jobcode.blocked')</button></a>";
                            else if(o.status == 'inactive')

                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("jobcode.inactive")</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('jobcode.active')</button></a>";

                            return status;

                        }

                    },
					{
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";

                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('jobcode.edit')</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> @lang('jobcode.delete')</button></a>&nbsp;";

                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('jobcode.view')</button></a>&nbsp;";


                            return v+e+d;
                        }
                    }

                ]
        });

    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/jobcode/') }}";
        url = url + "/" + id;
		//alert(url);
        var r = confirm("Are you sure you want to delete jobcode ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

</script>
@endpush
