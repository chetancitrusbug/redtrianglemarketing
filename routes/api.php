<?php

use Illuminate\Http\Request;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *, Origin, Content-Type, Authorization, X-Auth-Token, x-xsrf-token ');
header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1.0/', 'namespace' => 'Api'], function () {

    Route::post('user/login', 'UsersController@login');
    Route::post('user/forgot', 'UsersController@forgot');
    Route::post('user/registration', 'UsersController@register');

    Route::post('contact-us', 'UsersController@contactUs');
    Route::post('special-inquiry', 'UsersController@specialInquiry');

});

Route::group(['prefix' => 'v1.0', 'middleware' => ["api_pass"], 'namespace' => 'Api'], function () {
    //Route::get('getjob','JobsController@index');
    Route::post('user/changepassword', 'UsersController@changePassword');
    Route::post('user/update', 'UsersController@update');
    Route::post('user/logout', 'UsersController@logout');
    Route::get('jobs/joblist', 'JobsController@joblist');
    Route::get('jobs/jobdetail', 'JobsController@jobdetail');
    Route::get('jobs/jobtasklist', 'JobsController@jobtasklist');
    Route::get('jobs/jobfolderlist', 'JobsController@jobfolderlist');
	Route::get('jobs/jobtaskfolderlist', 'JobsController@jobtaskfolderlist');
    Route::get('jobs/foldervideodetail', 'JobsController@foldervideodetail');
    Route::get('jobs/folderdocumentdetail', 'JobsController@folderdocumentdetail');
    Route::get('jobs/videodetail', 'JobsController@videodetail');
    Route::get('jobs/documentdetail', 'JobsController@documentdetail');
    Route::post('jobs/createjobcard', 'JobsController@createjobcard');
    Route::get('jobs/jobcardlist', 'JobsController@jobcardlist');
    Route::get('jobs/jobcarddetail', 'JobsController@jobcarddetail');
    Route::post('jobs/addimage', 'JobsController@addimage');
    Route::post('jobs/adddocument', 'JobsController@adddocument');
    Route::post('jobs/addvideo', 'JobsController@addvideo');
    Route::get('products/productlist', 'ProductsController@productlist');
    Route::get('products/productdetail', 'ProductsController@productdetail');
    Route::post('jobs/updatejobtask', 'JobsController@updatejobtask');
    Route::post('jobs/markasdone', 'JobsController@markasdone');
    Route::post('jobs/addnotes', 'JobsController@addnotes');
	 Route::post('jobs/editnotes', 'JobsController@editnotes');
    Route::post('jobs/deletenotes', 'JobsController@deletenotes');
    Route::get('jobs/noteslist', 'JobsController@notesfolderlist');
	Route::get('jobs/archievejoblist', 'JobsController@jobsDoneList');
    Route::post('products/productinquiry', 'ProductsController@productinquiry');

    //Job Geothermal Form
    Route::get('jobs/jobGeothermalFormList', 'JobsController@jobGeothermalFormList');
    Route::post('jobs/addJobGeothermalForm', 'JobsController@addJobGeothermalForm');
    Route::post('jobs/deleteJobGeothermalForm', 'JobsController@deleteJobGeothermalForm');
    Route::post('jobs/editJobGeothermalForm', 'JobsController@editJobGeothermalForm');

    // Material
    Route::get('listMaterial', 'MaterialController@listMaterial');
    Route::get('materialDetail', 'MaterialController@materialDetail');
    Route::get('getMaterialUsed', 'MaterialController@getMaterialUsed');


    // Job
    Route::get('checkInOutJob', 'JobsController@checkInOutJob');


    //Tools
    Route::get('checkInOutTools', 'ProductsController@checkInOutTools');
    Route::get('toolsAvailableList', 'ProductsController@toolsAvailableList');
    Route::get('toolsUsedList', 'ProductsController@toolsUsedList');






});
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
// });

// Route::group(['middleware' => ['auth', '']], function () {

// });

// Route::group(['prefix' => 'v1.0/', 'middleware' => 'api_pass', 'namespace' => 'Api'], function () {
// //     //
// //     Route::get('/get-subjects', 'TicketController@getSubjects');
// //     Route::get('/get-subject/{subject_id}', 'TicketController@getSubject');
// //     //
// //     Route::get('/get-tickets', 'TicketController@getTickets');
// //     Route::get('/get-ticket/{ticket_id}', 'TicketController@getTicket');
// // //    //
// //     Route::post('/create-ticket', 'TicketController@postTicket');
// //     Route::put('/edit-ticket/{ticket_id}', 'TicketController@putTicket');
// //     //
// //     Route::delete('/delete-ticket/{ticket_id}', 'TicketController@deleteTicket');


// });

function make_null($value){
    $value = $value->toArray();
    array_walk_recursive($value, function (&$item, $key) {
        $item =  $item === null ? "" : $item;
    });
    return $value;

}