<style>
    .middle
    {
        margin: auto;
        text-align: center;
        font-weight:bold;
    }
</style>


@if(isset($material->material_name))
<div class="form-group{{ $errors->has('material_name') ? ' has-error' : ''}}">
    {!! Form::label('material_name', trans('material.material_name'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('material_name', null, ['class' => 'form-control','disabled' => 'disabled']) !!}
        {!! $errors->first('material_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@else
<div class="form-group{{ $errors->has('material_name') ? ' has-error' : ''}}">
    {!! Form::label('material_name', trans('material.material_name'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('material_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('material_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif

<div class="form-group{{ $errors->has('min_stock') ? ' has-error' : ''}}">
    {!! Form::label('min_stock', trans('material.min_stock'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::number('min_stock', null, ['class' => 'form-control']) !!}
        {!! $errors->first('min_stock', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(isset($material->stock))
<div class="row">
    <p class="middle">Available Stock :- {{ $material->stock }}</p><br>
</div>  
@endif

<div class="form-group{{ $errors->has('addstock') ? ' has-error' : ''}}">
    {!! Form::label('addstock', trans('material.addstock'), ['class' => 'col-md-4 control-label ']) !!}
    <div class="col-md-6">
        {!! Form::number('addstock', null, ['class' => 'form-control']) !!}
        {!! $errors->first('addstock', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('material.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

