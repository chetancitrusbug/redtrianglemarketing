<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Folder;

class FolderController extends Controller
{
   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function add(Request $request)
    {
    if($request->input('folder_name'))
    {
            $foldercheck=Folder::where('job_id',$request->input('job_id'))->where('folder_name',$request->input('folder_name'))->get();
            
            if(count($foldercheck)>0){
              return json_encode(array('msg'=>'Fail'));
              exit;
            }
            else{
               $folder = new Folder();
                $folder->job_id = $request->input('job_id');
                $folder->folder_name = $request->input('folder_name');
                if($request->input('folder_name')=='Electrical'){
                   $folder->sortno='1';
                }else if($request->input('folder_name')=='Heating'){
                   $folder->sortno='2';
                }else if($request->input('folder_name')=='Cooling'){
                   $folder->sortno='3';
                }else if($request->input('folder_name')=='Plumbing'){
                   $folder->sortno='4';
                }else if($request->input('folder_name')=='Other'){
                   $folder->sortno='5';
                }else{
                   $folder->sortno='6';
                }
                $user_id=\Auth::user()->id;
                $folder->employee_id=$user_id;
                $folder->user_type='admin';
                $folder->folder_img='';
                $folder->save();
                $folder->created_by=$folder->id;
                $folder->save();

             return json_encode(array('msg'=>'Success','data'=>$folder));
             exit;
            }
    }

		
    }


}
