{{-- <div class="form-group{{ $errors->has('job_builder_id') ? ' has-error' : ''}}">
    {!! Form::label('job_builder_id', trans('job.builder'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">

         {!! Form::select('job_builder_id',$builder, isset($job_builder_id)?$job_builder_id:null, ['class' => 'form-control']) !!}
         {!! $errors->first('job_builder_id', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div> --}}
<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', trans('job.title'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('job_number') ? ' has-error' : ''}}">
    {!! Form::label('job_number', trans('job.job_number'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('job_number', null, ['class' => 'form-control']) !!}
        {!! $errors->first('job_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('job.description'), ['class' => 'col-md-4 control-label required']) !!}
     <div class="col-md-6">
     {!! Form::textarea('description', null, ['class' => 'form-control','size' => '30x5']) !!}
    {!! $errors->first('description', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('client_id') ? ' has-error' : ''}}">
    {!! Form::label('client_id', trans('job.client'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">

         {!! Form::select('client_id',$client, null, ['class' => 'form-control']) !!}
         {!! $errors->first('client_id', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('job_employee_id') ? ' has-error' : ''}}">
    {!! Form::label('job_employee_id', trans('job.empse'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
            @if(Route::currentRouteName() == 'job.edit')
                <select id="job_employee_id" class="form-control" name="job_employee_id[]" multiple>
                    @foreach($user as $users)
                        <option value="{{$users->id}}"   @foreach($jobassignemp as $jobemp) @if($users->id == $jobemp->job_employee_id)selected="selected"@endif @endforeach>{{$users->name}}</option>
                    @endforeach
                </select>
           @else
                <select id="job_employee_id" class="form-control" name="job_employee_id[]" multiple>
                    @foreach($user as $users)
                        <option value="{{$users->id}}">{{$users->name}}</option>
                    @endforeach
                </select>
            @endif
    </div>
</div>

<div class="form-group{{ $errors->has('duedate') ? ' has-error' : ''}}">
    {!! Form::label('duedate', trans('job.duedate'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('duedate', null, ['class' => 'form-control', 'placeholder' => 'MM-DD-YYYY','id' => 'duedate']) !!}
        {!! $errors->first('duedate', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('notes') ? 'has-error' : ''}}">
    {!! Form::label('notes', trans('job.notes'), ['class' => 'col-md-4 control-label required']) !!}
     <div class="col-md-6">
     {!! Form::textarea('notes', null, ['class' => 'form-control','size' => '30x5']) !!}
    {!! $errors->first('notes', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

@if(!isset($job))
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">@lang('job.heating')</div>
        {{-- <div class="form-group{{ $errors->has('heating') ? ' has-error' : ''}}">
            {!! Form::label('heating', trans('job.heating'),['class' => 'col-md-4 control-label ']) !!}
            <div class="col-md-6">
                <input type="text" class="form-control" disabled name="heating" value="heating" />
            </div>
        </div> --}}
        <br><br>
        <div class="form-group{{ $errors->has('heating_documents') ? ' has-error' : ''}}">
            {!! Form::label('heating_documents', trans('job.adddocument'),['class' => 'col-md-4 control-label ']) !!}
            <div class="col-md-6">
                <input type="file" class="form-control" name="heating_documents[]" multiple /> {!! $errors->first('heating_documents', '
                <p class="help-block with-errors">:message</p>') !!}
            </div>
        </div>
        <div class="form-group{{ $errors->has('heating_videos') ? ' has-error' : ''}}">
            {!! Form::label('heating_videos', trans('job.addvideo'),['class' => 'col-md-4 control-label ']) !!}
            <div class="col-md-6">
                <input type="file" class="form-control" name="heating_videos[]" multiple /> {!! $errors->first('heating_videos',
                '
                <p class="help-block with-errors">:message</p>') !!}
            </div>
        </div>
</div>
</div>


<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">@lang('job.cooling')</div>
    {{--
    <div class="form-group{{ $errors->has('cooling') ? ' has-error' : ''}}">
        {!! Form::label('cooling', trans('job.cooling'),['class' => 'col-md-4 control-label ']) !!}
        <div class="col-md-6">
            <input type="text" class="form-control" disabled name="cooling" value="cooling" />
        </div>
    </div> --}}
    <br><br>
    <div class="form-group{{ $errors->has('cooling_documents') ? ' has-error' : ''}}">
        {!! Form::label('cooling_documents', trans('job.adddocument'),['class' => 'col-md-4 control-label ']) !!}
        <div class="col-md-6">
            <input type="file" class="form-control" name="cooling_documents[]" multiple /> {!! $errors->first('cooling_documents', '
            <p class="help-block with-errors">:message</p>') !!}
        </div>
    </div>


    <div class="form-group{{ $errors->has('cooling_videos') ? ' has-error' : ''}}">
        {!! Form::label('cooling_videos', trans('job.addvideo'),['class' => 'col-md-4 control-label ']) !!}
        <div class="col-md-6">
            <input type="file" class="form-control" name="cooling_videos[]" multiple /> {!! $errors->first('cooling_videos', '
            <p class="help-block with-errors">:message</p>') !!}
        </div>
    </div>

</div>
</div>

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">@lang('job.plumbing')</div>

        {{-- <div class="form-group{{ $errors->has('plumbing') ? ' has-error' : ''}}">
            {!! Form::label('plumbing', trans('job.plumbing'),['class' => 'col-md-4 control-label ']) !!}
            <div class="col-md-6">
                <input type="text" class="form-control" disabled name="plumbing" value="plumbing" />
            </div>
        </div> --}}
        <br><br>
        <div class="form-group{{ $errors->has('plumbing_documents') ? ' has-error' : ''}}">
            {!! Form::label('plumbing_documents', trans('job.adddocument'),['class' => 'col-md-4 control-label ']) !!}
            <div class="col-md-6">
                <input type="file" class="form-control" name="plumbing_documents[]" multiple /> {!! $errors->first('plumbing_documents', '
                <p class="help-block with-errors">:message</p>') !!}
            </div>
        </div>


        <div class="form-group{{ $errors->has('plumbing_videos') ? ' has-error' : ''}}">
            {!! Form::label('plumbing_videos', trans('job.addvideo'),['class' => 'col-md-4 control-label ']) !!}
            <div class="col-md-6">
                <input type="file" class="form-control" name="plumbing_videos[]" multiple /> {!! $errors->first('plumbing_videos', '
                <p class="help-block with-errors">:message</p>') !!}
            </div>
        </div>
</div>
</div>
@endif


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('client.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@push('script-head')
<script type="text/javascript">
   $(function () {
        $('#duedate').datetimepicker({
            format: 'MM-DD-YYYY',

        });
        $("#duedate").on("dp.change", function (e) {
            format: 'MM-DD-YYYY',
            $('#duedate').data("DateTimePicker").minDate(e.date);
        });
    })
</script>
@endpush