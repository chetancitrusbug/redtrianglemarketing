
<div class="form-group{{ $errors->has('job_number') ? ' has-error' : ''}}">
    {!! Form::label('job_number', trans('client.job_number'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('job_number', null, ['class' => 'form-control']) !!}
        {!! $errors->first('job_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', trans('client.name'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email',trans('client.email'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('street1') ? ' has-error' : ''}}">
    {!! Form::label('street1',trans('client.street1'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::textarea('street1', null, ['class' => 'form-control','size' => '30x5']) !!}
        {!! $errors->first('street1', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('city') ? 'has-error' : ''}} ">
    {!! Form::label('city', trans('client.city'), ['class' => ' col-md-4 control-label required']) !!}
    <div class="col-md-6">
       {!! Form::text('city', null, ['class' => 'form-control']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>

</div>
@if(Route::currentRouteName() == 'clients.edit')

{{--<div class="form-group{{ $errors->has('country') ? ' has-error' : ''}} ">
    {!! Form::label('country', trans('client.country'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
	 {!!Form::select('country',$countries,$country_id,array('class'=>'form-control selectTag','id'=>'country'));!!}
       {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
     </div>
</div>--}}

<div class="form-group{{ $errors->has('province') ? ' has-error' : ''}}">

    {!! Form::label('province', trans('client.province'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::select('province',$states,$state_id, ['class' => 'form-control  selectTag','id'=>'state']) !!}

        {!! $errors->first('province', '<p class="help-block">:message</p>') !!}

    </div>
</div>

@else
 {!! Form::hidden('country', '1', ['class' => 'form-control']) !!}
{{--<div class="form-group{{ $errors->has('country') ? ' has-error' : ''}} ">
    {!! Form::label('country', trans('client.country'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
	 {!!Form::text('country',$countries,'null',array('class'=>'form-control selectTag','id'=>'country'));!!}
       {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
     </div>
</div>--}}

<div class="form-group{{ $errors->has('province') ? ' has-error' : ''}}">

    {!! Form::label('province', trans('client.province'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::select('province', $states,'null', ['class' => 'form-control  selectTag','id'=>'state']) !!}
        {!! $errors->first('province', '<p class="help-block">:message</p>') !!}

    </div>
</div>

@endif

<div class="form-group {{ $errors->has('zipcode') ? 'has-error' : ''}} ">
    {!! Form::label('zipcode', trans('client.zipcode'), ['class' => ' col-md-4 control-label required']) !!}
    <div class="col-md-6">
       {!! Form::text('zipcode', null, ['class' => 'form-control']) !!}
        {!! $errors->first('zipcode', '<p class="help-block">:message</p>') !!}
    </div>

</div>
<div class="form-group{{ $errors->has('street2') ? ' has-error' : ''}}">
    {!! Form::label('street2',trans('client.street2'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
         {!! Form::textarea('street2', null, ['class' => 'form-control','size' => '30x5']) !!}
        {!! $errors->first('street2', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('bill_city') ? 'has-error' : ''}} ">
    {!! Form::label('bill_city', trans('client.city'), ['class' => ' col-md-4 control-label required']) !!}
    <div class="col-md-6">
       {!! Form::text('bill_city', null, ['class' => 'form-control']) !!}
        {!! $errors->first('bill_city', '<p class="help-block">:message</p>') !!}
    </div>

</div>
@if(Route::currentRouteName() == 'clients.edit')

<div class="form-group{{ $errors->has('bill_state') ? ' has-error' : ''}}">

    {!! Form::label('bill_state', trans('client.province'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::select('bill_state',$states,$bstate_id, ['class' => 'form-control  selectTag','id'=>'states']) !!}

        {!! $errors->first('bill_state', '<p class="help-block">:message</p>') !!}

    </div>
</div>

@else

<div class="form-group{{ $errors->has('bill_state') ? ' has-error' : ''}}">

    {!! Form::label('bill_state', trans('client.province'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::select('bill_state', $states,'null', ['class' => 'form-control  selectTag','id'=>'states']) !!}
        {!! $errors->first('bill_state', '<p class="help-block">:message</p>') !!}

    </div>
</div>

@endif

<div class="form-group {{ $errors->has('bill_zipcode') ? 'has-error' : ''}} ">
    {!! Form::label('bill_zipcode', trans('client.zipcode'), ['class' => ' col-md-4 control-label required']) !!}
    <div class="col-md-6">
       {!! Form::text('bill_zipcode', null, ['class' => 'form-control']) !!}
        {!! $errors->first('bill_zipcode', '<p class="help-block">:message</p>') !!}
    </div>

</div>
<div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
    {!! Form::label('phone', trans('client.phone'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('phone', null, ['class' => 'form-control','id' => 'phone','placeholder'=>'']) !!}
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
{{-- <div class="form-group{{ $errors->has('client_builder_id') ? ' has-error' : ''}}">
    {!! Form::label('client_builder_id', trans('client.builder'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">

         {!! Form::select('client_builder_id',$builder, isset($client_builder_id)?$client_builder_id:null, ['class' => 'form-control']) !!}
         {!! $errors->first('client_builder_id', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div> --}}
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('client.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@push('script-head')
<script type="text/javascript">
$('#phone').mask('0000000000');
    $("#formClient").validate({

        rules: {
            job_number: {
                required: true
            },
            name: {
                required: true
            },
            street1: {
                required: true
            },
            street2: {
                required: true
            },
            // country: {
            //     required: true
            // },
            province: {
                required: true
            },
            city: {
                required: true
            },
            bill_state: {
                required: true
            },
            bill_city: {
                required: true
            },
            bill_zipcode: {
                required: true
            },
            email: {
                required: true
            },
            zipcode: {
                required: true
            },
            phone: {
                required: true
            },
            client_builder_id: {
                required: true
            }
        },
        messages: {
            job_number: {
                required: "Job Number is required"
            },
            name: {
                required: "Name is required"
            },
            street1: {
                required: "Job Site Address is required"
            },
            street2: {
                required: "Billing Address is required"
            },
            // country: {
            //     required: "Country is required"
            // },
            province: {
                required: "State is required"
            },
            city: {
                required: "City is required"
            },
            bill_state: {
                required: "State is required"
            },
            bill_city: {
                required: "City is required"
            },
            email: {
                required: "Email is required"
            },
            zipcode: {
                required: "Zipcode is required"
            },
            bill_zipcode: {
                required: "Zipcode is required"
            },
            phone: {
                required: "Phone No is required"
            },
            client_builder_id: {
                required: "Builder is required"
            }

        },
        submitHandler: function(form) {
            form.submit();
        }
    });
// 	$( document ).ready(function() {

//     var countryID = $('#country').val();
//     if(countryID){
//         $.ajax({
//            type:"GET",
//            url:"{{url('admin/api/get-state-list')}}?country_id="+countryID,

//            success:function(res){
//             if(res){
//                 $("#state").empty();
//                 $("#state").append('<option>Select</option>');
//                 $.each(res,function(key,value){
//                     $("#state").append('<option value="'+key+'">'+value+'</option>');
//                 });

//             }else{
//                $("#state").empty();
//             }
//            }
//         });
//     }else{
//         $("#state").empty();
//         $("#city").empty();
//     }
// });
//     $('#country').change(function(){
//     var countryID = $(this).val();
//     if(countryID){
//         $.ajax({
//            type:"GET",
//            url:"{{url('admin/api/get-state-list')}}?country_id="+countryID,

//            success:function(res){
//             if(res){
//                 $("#state").empty();
//                 $("#state").append('<option>Select</option>');
//                 $.each(res,function(key,value){
//                     $("#state").append('<option value="'+key+'">'+value+'</option>');
//                 });

//             }else{
//                $("#state").empty();
//             }
//            }
//         });
//     }else{
//         $("#state").empty();
//         $("#city").empty();
//     }
//    });
//     $('#state').on('change',function(){
//     var stateID = $(this).val();
//     if(stateID){
//         $.ajax({
//            type:"GET",
//            url:"{{url('admin/api/get-city-list')}}?state_id="+stateID,
//            success:function(res){
//             if(res){
//                 $("#city").empty();
//                 $.each(res,function(key,value){
//                     $("#city").append('<option value="'+key+'">'+value+'</option>');
//                 });

//             }else{
//                $("#city").empty();
//             }
//            }
//         });
//     }else{
//         $("#city").empty();
//     }

//    });
</script>
@endpush
