<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Document;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use App\Folderdocument;

class DocumentController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        return view('admin.documents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {

        return view('admin.documents.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {

    $rules = [
        'title' => 'required',
        'description' => 'required',
        'document_url' => 'required',
    ];

    $customMessages = [
         'document_url.required' => 'Please Upload Document',
    ];

    $this->validate($request, $rules, $customMessages);
		$data = $request->all();
        if ($request->file('document_url')) {

            $document_url = $request->file('document_url');
            $filename = uniqid(time()) . '.' . $document_url->getClientOriginalExtension();

            $document_url->move(public_path('Document'), $filename);

            $data['document_url'] = $filename;

        }
        $data['user_type']='admin';
        $document = Document::create($data);

        Session::flash('flash_message', 'Document added!');

        return redirect('admin/documents');
    }

    public function datatable(request $request)
    {
        $document = Document::where('user_type','admin')->orderBy('id','desc')->select('*');

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(title LIKE  '%$value%')";

               $document= Document::whereRaw($where_filter);
            }
        }

        return Datatables::of($document)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $document = Document::find($id);

        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $document->status= 'inactive';
                $document->update();

                return redirect()->back();
            }else{
                $document->status= 'active';
                $document->update();
                return redirect()->back();
            }

        }
		if($document){
            return view('admin.documents.show', compact('document'));
        }
        else{
             return redirect('/admin/documents');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $request->id=$id;
		$document = Document::where('id',$id)->first();

        return view('admin.documents.edit', compact('document'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[

            'title' => 'required',
            'description' => 'required'
        ]);
        $requestData = $request->all();

        $document = Document::findOrFail($id);

        if ($request->file('document_url')) {

            $document_url = $request->file('document_url');
            $filename = uniqid(time()) . '.' . $document_url->getClientOriginalExtension();

            $document_url->move(public_path('Document'), $filename);
        }


		if(isset($filename)){
			 $requestData['document_url'] = $filename;
		}

	    $document->update($requestData);

        flash('Document Updated Successfully!');

        return redirect('admin/documents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

        $document = Document::find($id);
        $folderdocument = Folderdocument::where('document_id',$id)->get();
        foreach($folderdocument as $deldocument){
            $deldocument->delete();
        }
        $document->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);
    }

}
