<!-- / START - page related stylesheets [optional] -->
<link rel="stylesheet" href="{!! asset('/assets/stylesheets/plugins/fuelux/wizard.css') !!}">
<!-- / bootstrap [required] -->
<link href="{!! asset('assets/stylesheets/bootstrap/bootstrap.css') !!}" media="all" rel="stylesheet"
      type="text/css"/>
<link href="{!! asset('assets/stylesheets/plugins/select2/select2.css') !!}" media="all"
      rel="stylesheet"
      type="text/css"/>

<link href="{!! asset('css/bootstrap-datetimepicker.css') !!}"
      media="all"
      rel="stylesheet"
      type="text/css"/>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
<!-- / theme file [required] -->
<link href="{!! asset('assets/stylesheets/lighttheme.css') !!}" media="all" id="color-settings-body-color"
      rel="stylesheet" type="text/css"/>
<!-- / coloring file [optional] (if you are going to use custom contrast color) -->
<link href="{!! asset('assets/stylesheets/theme-colors.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<!-- / demo file [not required!] -->
<link href="{!! asset('assets/stylesheets/demo.css') !!}" media="all" rel="stylesheet" type="text/css"/>


<link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link href="{!! asset('assets/stylesheets/design.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<link href="{!! asset('assets/stylesheets/style.css') !!}" media="all" rel="stylesheet" type="text/css"/>
 <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
 <link href="{!! asset('assets/stylesheets/jquery/jquery-ui.css') !!}" rel="stylesheet" type="text/css"/>