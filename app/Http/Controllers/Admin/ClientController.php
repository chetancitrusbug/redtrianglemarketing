<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use App\Country;
use App\Job;
use App\Jobassignemployee;
use App\Jobtask;
use App\Folder;
use App\Folderdocument;
use App\Foldervideo;
use App\Builder;
use App\AssignclientBuilder;
use App\State;
class ClientController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        return view('admin.clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $countries =Country::pluck('name','id');
        //->prepend('Select Country','');
        $states =State::where('country_id','1')->pluck('name','id')->prepend('Select State','');
        $builder = Builder::pluck('name','id')->prepend('Select Builder','');
        return view('admin.clients.create',compact('countries','builder','states'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|unique:clients',
        ]);

		$data = $request->all();

         //if(!empty($request->input('client_builder_id'))){
                 $client = Client::create($data);
                 $client->created_by=$client->id;
                     $client->save();

            //             $assignclientbuilder = new AssignclientBuilder();
            //             $assignclientbuilder->client_id = $client->id;
            //             $assignclientbuilder->client_builder_id = $request->input('client_builder_id');
            //             $assignclientbuilder->save();
            //  }

        Session::flash('flash_message', 'Client added!');

        return redirect('admin/clients');
    }

     public function datatable(request $request)
    {
        $client =  Client::with('assignbuilderid')->select('*')->orderBy('id','desc');
        if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(clients.name LIKE  '%$value%' OR clients.job_number LIKE  '%$value%'  )";
                $client = Client::with('assignbuilderid')->whereRaw($where_filter);
            }
        }
        return Datatables::of($client)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $client = Client::where('id',$id)->with('stateName','countryName','billstateName','assignbuilderid')->first();
		//dd($client);
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $client->status= 'inactive';
                $client->update();
                return redirect()->back();
            }else{
                $client->status= 'active';
                $client->update();
                return redirect()->back();
            }

        }

		if($client){
            return view('admin.clients.show', compact('client'));
        }
        else{
             return redirect('/admin/clients');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {

        $request->id=$id;
		$client = Client::where('id',$id)->first();
        $countries = DB::table("countries")->pluck('name','id')->prepend('Select Country','');
        $country_id=$client->country;
        //$city_id=$client->city;
        //$state_id=$client->province;

        $states =State::where('country_id','1')->pluck('name','id');
        $state_id=$client->province;
        $bstate_id=$client->bill_state;
        $states = DB::table("states")
                    ->where("country_id",$country_id)
                    ->pluck("name","id");
        $builder = Builder::pluck('name','id');
        $assignclientbuilder = AssignclientBuilder::where('client_id',$id)->first();

        $client_builder_id=isset($assignclientbuilder->client_builder_id) ? $assignclientbuilder->client_builder_id : '';
        // $cities = DB::table("cities")
        //             ->where("state_id",$state_id)
        //             ->pluck("name","id");
        return view('admin.clients.edit', compact('client','country_id','state_id','bstate_id','states','countries','builder','client_builder_id'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {

        $this->validate($request,[

           'name' => 'required',
            'street1' => 'required',
            'street2' => 'required',
            'province' => 'required',
            'city' => 'required',
            'bill_state' => 'required',
            'bill_city' => 'required',
            'bill_zipcode' => 'required',
            'zipcode' => 'required',
            'email' => 'required|unique:clients,email,' . $id,
            'phone' => 'required'
        ]);
        $requestData = $request->all();
        $client = Client::findOrFail($id);
	    $client->update($requestData);
        // $assignclientbuilder = AssignclientBuilder::where('client_id',$id);
        // $assignclientbuilder->delete();
        //                 $assignclientbuilder = new AssignclientBuilder();
        //                 $assignclientbuilder->client_id = $client->id;
        //                 $assignclientbuilder->client_builder_id = $request->input('client_builder_id');
        //                 $assignclientbuilder->save();

        flash('Client Updated Successfully!');

        return redirect('admin/clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

        $client = Client::find($id);
        $client->delete();

        $job = Job::where('client_id',$id)->first();
		if($job){
			$jobassignemployee = Jobassignemployee::where('job_id',$job->id)->get();
			foreach($jobassignemployee as $delemp){
				  $delemp->delete();
			}
			$jobtask = Jobtask::where('job_id',$job->id)->get();
			foreach($jobtask as $deltask){
				$deltask->delete();
			}
			$folder = Folder::where('job_id',$job->id)->get();
			foreach($folder as $delfolder){
				$delfolder->delete();
			}
			$folderdocument = Folderdocument::where('job_id',$job->id)->get();
			foreach($folderdocument as $delfoldoc){
				$delfoldoc->delete();
			}
			$foldervideo = Foldervideo::where('job_id',$job->id)->get();
			foreach($foldervideo as $delfolvideo){
				 $delfolvideo->delete();
			}

			$job->delete();
		}
          $message='Deleted';
        return response()->json(['message'=>$message],200);
    }

	public function downloadsamplecsv(){

        /* Generate Csv File */
        $filename = "file.csv";
        $handle = fopen($filename, 'w+');
        $data = array('Job Number','Name','Email','Job Site Address','City','State','Zipcode','Billing Address','City','State','Zipcode','Phone');
        $value = array('J-123','Json','Json@gmail.com','3340 Brown Avenue','Anderson','South Carolina','29621','557 Park Ave','Tyrone','South Carolina','296213','864-964-4508');

        fputcsv($handle, $data);

            fputcsv($handle, $value);

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );
        return response()->download($filename, 'sample '.date("d-m-Y H:i").'.csv', $headers);
        return redirect()->back();
    }

    public function uploadclientcsv(){
        return view ('admin.clients.createclient');
    }

    public function bulkuploadClientCsv(){

        if($_FILES['file']['name']){
            $clients = array_map('str_getcsv', file($_FILES['file']['tmp_name']));

            unset($clients[0]);
            foreach($clients as $key => $client){
                $clientExist = Client::where('email',$client[2])->first();
                $stateId = State::where('name',$client[5])->first();
               // $builderId = Builder::where('name',$client[12])->first();
                $billstateId = State::where('name',$client[9])->first();
                if(!$clientExist){

                        $clientdata = new Client();
                        $clientdata->job_number=$client[0];
                        $clientdata->name=$client[1];
                        $clientdata->email=$client[2];
                        $clientdata->street1=$client[3];
                        $clientdata->country='1';
                        $clientdata->province=$stateId->id;
                        $clientdata->city=$client[4];
                        $clientdata->phone=$client[11];
                        $clientdata->street2=$client[7];
                        $clientdata->bill_city=$client[8];
                        $clientdata->bill_state=$billstateId->id;
                        $clientdata->bill_zipcode=$client[10];
                        $clientdata->zipcode=$client[6];
                        $clientdata->save();
                            // $assignclientbuilder = new AssignclientBuilder();
                            // $assignclientbuilder->client_id = $clientdata->id;
                            // $assignclientbuilder->client_builder_id = '1';
                            // $assignclientbuilder->save();
                            Session::flash('flash_message', 'Imported Successfully');
                }
            }


            return redirect()->back();
        }
    }

}
