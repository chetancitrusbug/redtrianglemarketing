<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Session;
use App\Job;
use App\Jobassignemployee;
use App\User;
use Date;
use App\AssignjobBuilder;

use Dedicated\GoogleTranslate\Translator;

class AdminController extends Controller
{
  
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        if(isset($request->emp_id) && $request->emp_id != 0)
        {
            $job_ids = [];
            $vars = Jobassignemployee::where('job_employee_id',$request->emp_id)->get();
             foreach($vars as $var)
             {
                $job_ids[] = $var->job_id;
             }
             $job_ids = array_unique($job_ids);
    
            // $jobs = Job::whereIn('id',$job_ids)->get();
             $jobs = jobassignemployee::join('job', 'jobassignemployee.job_id', '=', 'job.id')
             ->join('users', 'jobassignemployee.job_employee_id', '=', 'users.id')
             ->whereIn('job.id',$job_ids)
             ->where('job.status','active')
            ->get();

        }
        else{
           $jobs = jobassignemployee::join('job', 'jobassignemployee.job_id', '=', 'job.id')
           ->join('users', 'jobassignemployee.job_employee_id', '=', 'users.id')
           ->where('job.status','active')
          ->get();
        }
    
        $events = [];
         $name= [];
         //dd($jobs);
        foreach($jobs as $job){
           $name = array();  
           if(isset($job->job_employee))
           {
                foreach($job->job_employee as $emp_name){
                    $name[] = $emp_name->name; 
                }
           }
           else
           {
                $name[] = $job->name;
           }
           //dd($name);
           $emp_name= implode(",",$name);
         
            $tmp = new \stdClass();
            if(count($name) > 1){
                $tmp->content ='Job:'.' '.$job->title.'\n'.'Employees:'.' '.$emp_name;
            }
            else{
                 $tmp->content ='Job:'.' '.$job->title.'\n'.'Employee:'.' '.$emp_name;
            }
           
           // $end_d = date("Y, m, d", strtotime($job->duedate));
		  
			$date_of_birth = explode('-', $job->duedate);
            $month = $date_of_birth[0];
            $day   = $date_of_birth[1];
			$year  = $date_of_birth[2];
			$duedate=$year.'-'.$month.'-'.$day;			
			$tmpdateYear = date("Y", strtotime($duedate));
 
			$tmpdateMonth = date("m", strtotime($duedate));
			  
			$tmpdateDay = date("d", strtotime($duedate));
			  
			$end_d = $tmpdateYear.", ".($tmpdateMonth-1).", ".$tmpdateDay;
			
			$tmpsdateYear = date("Y", strtotime($duedate));
 
			$tmpsdateMonth = date("m", strtotime($duedate));
		  
			$tmpsdateDay = date("d", strtotime($duedate));
		  
			$start_d = $tmpsdateYear.", ".($tmpsdateMonth-1).", ".$tmpsdateDay;
			
           // $start_d = date("Y, m, d", strtotime($job->duedate));
            if($job->start_time == null)
			{
				$s_time = str_replace(":",', ',  '00:00');
			}
			else{
				$s_time = str_replace(":",', ',  $job->start_time);
			}
			if($job->end_time == null)
			{
				$end_time = str_replace(":",', ',  '00:00');
			}
			else{
				$end_time = str_replace(":",', ',  $job->end_time);
			}
			
            //$end_time = str_replace(":",', ',  $job->end_time);
            $tmp->endDate = $end_d.', '.$end_time ;
            $tmp->startDate = $start_d.', '.$s_time;
           
            $events[] = $tmp;
        }

        $events = json_encode(($events));
         

        $employees = User::select('users.*','roles.name as role_name','roles.id as role_id','roles.label as role_label')
        ->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('roles.id','=','3')
        ->orderby('users.id','desc')
        ->get();
      

       return view('admin.dashboard', compact('employees','events'));
    }

    /**
     * Display given permissions to role.
     *
     * @return void
     */
    public function getGiveRolePermissions()
    {
        $roles = Role::with('permissions')->get();
//        return $roles;
        $permissions = Permission::with('child')->parent()->get();
        return view('admin.permissions.role-give-permissions', compact('roles', 'permissions'));
    }

    /**
     * Store given permissions to role.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function postGiveRolePermissions(Request $request)
    {
        $this->validate($request, ['role' => 'required', 'permissions' => 'required']);
        $role = Role::with('permissions')->whereName($request->role)->first();
        $role->permissions()->detach();
        foreach ($request->permissions as $permission_name) {
            $permission = Permission::whereName($permission_name)->first();
            $role->givePermissionTo($permission);
        }
        Session::flash('flash_success', _('Permission granted!'));
        return redirect('admin/roles');
    }

    public function getLangTranslation($to,Request $request){
        if (!$request->has('filename')) {
            echo "not valid file "; exit;
        }
        \App::setLocale('en');
        $langs = \Lang::get($request->filename);
        print "--------------------------------".$request->filename."-------------------------------------------<br><br><br>";
        foreach ($langs as $key=>$val){
            if(is_array($val)){
                foreach ($val as $k=>$s){
                    if(is_array($s)){
                        foreach ($s as $d=>$j){
                            if(!is_array($j)) $langs[$key][$k][$d] = $this->doTranslator('en',$to,$j);
                        }

                    }else{
                        $langs[$key][$k] = $this->doTranslator('en',$to,$s);
                    }

                }
            }else{
                $langs[$key] = $this->doTranslator('en',$to,$val);
            }
        }
        print "<pre>";
        var_export($langs);
        print "<br><br><br>---------------------------------------------------------------------------";
        exit;
    }

    public function doTranslator($from,$to,$data){
        $translator = new Translator;
        try{
            $data = $translator->setSourceLang($from)
                ->setTargetLang($to)
                ->translate($data);

        }catch (\Exception $e){
            //$desc =  $e->getMessage();
        }
        return html_entity_decode($data, ENT_QUOTES, "utf-8");
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function adminlist(Request $request)
    {
        /* $customer_id = "";
        if($request->has('customer_id') && $request->get('customer_id') != "" ){

            $customer_id = $request->get('customer_id');
        }
 */
        $role = Role::pluck('label', 'id')->prepend('Filter Users by Roles', '');
       
        return view('admin.admin.list', compact('role'));
    }

    public function datatable(request $request)
    {
        $user = User::select('users.*','roles.name as role_name','roles.id as role_id','roles.label as role_label')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id');         

        if ($request->has('filter_role') && $request->get('filter_role') != '') {
            $user->where('roles.id', $request->get('filter_role'),'OR');
        }
         if ($request->has('filter_status') && $request->get('filter_status') != '' && $request->get('filter_status') != 'all') {
            $user->where('users.status', $request->get('filter_status'), 'OR');
        }  
         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(users.name LIKE  '%$value%' OR users.email LIKE  '%$value%'  )";
                $user->whereRaw($where_filter);
            }
        }     


        $user->groupBy('users.id');

        return Datatables::of($user)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {  
        
        $roles = Role::where('name','EMP');
        $roles = $roles->pluck('label', 'name');
        $role = $request->role;
        
        return view('admin.admin.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'roles' => 'required',
            
        ]);
		
        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);
		$data['status'] = 'active';
        $user = User::create($data);
		
        $user->assignRole($request->roles);
       

        Session::flash('flash_message', 'Job added!');

        return redirect('admin/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        //notification
        if(\Auth::check()){

            if(\Auth::user()->roles[0]->name == "SU"){
                //if user is added by admin, notification send to all backoffice

                $cbb = Role::Where('name','SU')->get();

                if($cbb != ""){
                    foreach($cbb as $roles){
                        foreach($roles->users as $cbbuser){
                            $user_id[] = $cbbuser->id ;
                        } 
                    }
                }
                $users = User::whereIN('id',$user_id)->get();

            }else{

                //if user is added by any backoffice then notification send to admin
                $su = Role::Where('name','SU')->get();

                if($su != ""){
                    foreach($su as $roles){
                        foreach($roles->users as $su_user){
                            $user_id[] = $su_user->id ;
                        } 
                    }
                }
                $users = User::whereIN('id',$user_id)->get();

            }
			
        }

        $user = User::findOrFail($id);
        
        //change users status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $user->status= 'inactive';
                $user->update();            

                return redirect()->back();
            }else{
                $user->user_status= 'active';
                $user->update();               
                return redirect()->back();
            }

        }

        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {

        $request->id=$id;
		$roles = Role::where('name','EMP');
        $roles = $roles->pluck('label', 'name');
		$user = User::where('id',$id)->first();

        return view('admin.users.edit', compact('user', 'roles'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
            
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $id
        ]);
        $requestData = $request->all();              
        $user = User::findOrFail($id);
	    $user->update($requestData);
        $user->roles()->detach();
        $user->assignRole($request->roles);

        flash('User Updated Successfully!');
		
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
       
        $user = User::find($id);
        $user->delete();

        return redirect('admin/users');
    }  
    public function assignJob(Request $request)
    {
      // print_r($request->all());exit;
        $this->validate($request, [
            'title' => 'required',
            'duedate' => 'required',
            'description'=>'required',
            'start_time' => 'required',
            'end_time' => 'required',
            
        ]);
        $data['title'] = $request->title;
        $data['duedate'] = $request->duedate;
        $data['description'] = $request->description;
        $data['start_time'] = $request->start_time;
        $data['end_time'] = $request->end_time;
      	$data['status'] = 'active';
        $data['job_status'] = 'Pending'; 
        $job = Job::create($data);
        $job->created_by=$job->id;
        $data['job_employee_id']= $request->job_employee_id;
        $data['job_id']= $job->id;
        $data['status']= 'active';
        $assignjobbuilder = new AssignjobBuilder();
                        $assignjobbuilder->job_id = $job->id;
                        $assignjobbuilder->job_builder_id = '1';
                        $assignjobbuilder->save();
        $Jobassignemployee = Jobassignemployee::create($data);
        
        $msg =  Session::flash('flash_message', 'Admin added!');
        return $msg;

    }
    public function employee_jobschedule(Request $request)
    {
        $events = [];
        $job_ids = [];
        $vars = Jobassignemployee::where('job_employee_id',$request->emp_id)->get();
         foreach($vars as $var)
         {
            $job_ids[] = $var->job_id;
         }
         $job_ids = array_unique($job_ids);

         $jobs = Job::whereIn('id',$job_ids)->get();
         foreach($jobs as $job){
            $tmp = new \stdClass();
            $tmp->content =$job->title;
            $end_d = str_replace("-",', ',  $job->duedate);
            $start_d = str_replace("-",', ',  $job->duedate);
            $s_time = str_replace(":",', ',  $job->start_time);
            $end_time = str_replace(":",', ',  $job->end_time);
            $tmp->endDate = $end_d.', '.$end_time ;
            $tmp->startDate = $start_d.', '.$s_time;
           
            $events[] = $tmp;
        }

        $events = json_encode(($events));
        // $employees = User::select('users.*','roles.name as role_name','roles.id as role_id','roles.label as role_label')
        // ->join('role_user', 'users.id', '=', 'role_user.user_id')
        // ->join('roles', 'roles.id', '=', 'role_user.role_id')
        // ->where('roles.id','=','3')
        // ->orderby('users.id','desc')
        // ->get();

        return $events;
    }
}
