@extends('layouts.backend')


@section('title',trans('client.add_client'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('client.add_new_client')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('client.add_new_client')</div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('user.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/clients', 'class' => 'form-horizontal','id'=>'formClient','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.clients.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection
