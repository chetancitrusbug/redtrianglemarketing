@extends('layouts.backend')

@section('title',trans('client.view_client'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('client.client')</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('client.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/clients/' . $client->id . '/edit') }}" title="Edit Client">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            @lang('client.edit_client')
                        </button>
                    </a>
                    @endif

                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
							<tr>
                                <td>@lang('client.job_number')</td>
                                <td>{{ $client->job_number }}</td>
                            </tr>

                            <tr>
                                <td>@lang('client.name')</td>
                                <td>{{ $client->name }}</td>
                            </tr>

                            <tr>
                                <td>@lang('client.email')</td>
                                <td>{{ $client->email }}</td>
                            </tr>

                            <tr>
                                <td>@lang('client.street1')</td>
                                <td>{{ $client->street1 }}</td>
                            </tr>
							 <tr>
                                <td>@lang('client.city')</td>
                                <td>{{ $client->city }}</td>
                            </tr>
                            <tr>
                                <td>@lang('client.province')</td>
                                <td>{{ ((isset($client->stateName->name)) ? $client->stateName->name : '-') }}</td>
                            </tr>
                            <tr>
                                <td>@lang('client.zipcode')</td>
                                <td>{{ $client->zipcode }}</td>
                            </tr>
                            <tr>
                                <td>@lang('client.street2')</td>
                                <td>{{ $client->street2 }}</td>
                            </tr>
							<tr>
                                <td>@lang('client.city')</td>
                                <td>{{ $client->bill_city }}</td>
                            </tr>
                            <tr>
                                <td>@lang('client.province')</td>
                                <td>{{ ((isset($client->billstateName->name)) ? $client->billstateName->name : '-') }}</td>
                            </tr>
                            <tr>
                                <td>@lang('client.zipcode')</td>
                                <td>{{ $client->bill_zipcode }}</td>
                            </tr>
                            <tr>
                                <td>@lang('client.phone')</td>
                                <td>{{ $client->phone }}</td>
                            </tr>
                            {{-- <tr>
                                <td>@lang('client.buildername')</td>
                                <td>{{ ((isset($client->assignbuilderid->builderName->name)) ? $client->assignbuilderid->builderName->name : '-') }}</td>
                            </tr>  --}}
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection