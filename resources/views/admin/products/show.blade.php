@extends('layouts.backend')

@section('title',trans('view_document'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('document.document')</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('document.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/products/' . $product->id . '/edit') }}" title="Edit Product">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            @lang('document.edit_document')
                        </button>
                    </a>
                    @endif
                    {!! Form::open([
                        'method' => 'DELETE',
                        'url' => ['/admin/products', $product->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Product',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('product.id')</td>
                                <td>{{ $product->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('product.name')</td>
                                <td>{{ $product->name }}</td>
                            </tr>
                       
                            <tr>
                                <td>@lang('product.description')</td>
                                <td>{{ $product->description }}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection