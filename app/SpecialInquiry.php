<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecialInquiry extends Model
{
    use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'special_inquiry';

    protected $fillable = ['note','file_1','file_2','file_3','file_4'];

    public function getFile1Attribute($value)
    {
        if($value != ""){
            return url('/').'/'.$value;
        }
        return $value;
    }

    public function getFile2Attribute($value)
    {
        if($value != ""){
            return url('/').'/'.$value;
        }
        return $value;
    }

    public function getFile3Attribute($value)
    {
        if($value != ""){
            return url('/').'/'.$value;
        }
        return $value;
    }

    public function getFile4Attribute($value)
    {
        if($value != ""){
            return url('/').'/'.$value;
        }
        return $value;
    }
}
