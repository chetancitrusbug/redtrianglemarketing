<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\EmailController;
use App\Job;
use App\User;
use App\Jobassignemployee;
use App\Jobtask;
use App\Folder;
use App\Foldervideo;
use App\Folderdocument;
use App\Video;
use App\Document;
use App\Jobcarddetail;
use App\Jobcard;
use App\Foldernotes;
use App\Taskfolder;
use App\JobGeothermalForm;
use App\JobLogs;
use DB;


class JobsController extends Controller
{
    public function __construct()
    {
        $this->mail_function = new EmailController();
    }

    public function checkInOutJob(Request $request){
        $data = [];
        $message = "Job logs Saved";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'in_out_flag'=>'required|integer',
            'date_time'=> 'required|date|date_format:Y-m-d H:i:s'
        );


        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {

            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $date_time = rawurldecode($request->date_time);
            $in_out_flag = $request->in_out_flag;
            //dd($date_time);
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();


            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }

            if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

            if($status){
                if ($request->in_out_flag == 0) {
                    $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                    if ($jobLogs != null) {
                        $status = false;
                        $message = 'Already Check In';
                        $code = 400;
                    }else {
                        $jobLogs = new JobLogs;
                        $jobLogs->job_id = $job_id;
                        $jobLogs->user_id = $user_id;
                        $jobLogs->start_datetime = $date_time;
                        $jobLogs->save();
                        $data = $jobLogs;
                    }
                }
                else {
                    $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $user_id)->first();
                    if($jobLogs != null)
                    {
                        $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $user_id)->where('end_datetime', '=', null)->first();
                        if ($jobLogs != null) {
                            $jobLogs->end_datetime = $date_time;
                            $jobLogs->save();
                            $data = $jobLogs;
                        } else {
                            $status = false;
                            $message = 'Already Check Out';
                            $code = 400;
                        }
                    }
                    else {
                        $status = false;
                        $message = 'No Job Log Found';
                        $code = 400;
                    }
                }
            }
        }
        return response()->json(['data'=>(object)$data, 'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function joblist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $page = $request->page;
        $users= User::where('users.id',$id)->first();
        if($users){
            $joblistdata = Job::join('jobassignemployee','job.id','=','jobassignemployee.job_id')
            ->where('jobassignemployee.job_employee_id',$id)
			->where('job.job_status','!=','markasdone')
            ->select('job.id as relation_id','job.title as title','job.description as description','job.duedate as duedate','job.notes as notes','job.status as status','job.deleted_at as deleted_at','job.created_by as created_by','job.created_at as created_at','job.updated_at as updated_at','job.client_id as client_id','job.job_status as job_status','job.job_number as job_number','job.start_time as start_time','job.end_time as end_time','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id');

            if ($request->search != ''){
                $search = $request->search;
                $joblistdata->where('job.title', 'like', $search.'%');
            }
            $joblistdata  = $joblistdata->paginate(10);

            if(count($joblistdata)>0){
                $data=$joblistdata;
            }
            else{
                $status = false;
                $message = 'No Jobs Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    public function jobdetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$id)->first();
        if($users){
             $jobdata = Job::with('clientName')
             ->where('id',$job_id)
             ->select('job.id as job_id','job.title as title','job.description as description','job.duedate as duedate','job.notes as notes','job.status as status','job.deleted_at as deleted_at','job.created_by as created_by','job.created_at as created_at','job.updated_at as updated_at','job.client_id as client_id','job.job_status as job_status','job.job_number as job_number','job.start_time as start_time','job.end_time as end_time')
            ->get();
            if(count($jobdata)>0){

                $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $id)->where('start_datetime','!=', null)->where('end_datetime','=', null)->first();
                    if ($jobLogs != null) {
                        $jobdata[0]->jonInOutStatus = 1;
                    }
                    else {
                        $jobdata[0]->jonInOutStatus = 0;
                    }
                 $data=$jobdata;
                 $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Jobs Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

     public function jobtasklist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
		$folder_id = $request->folder_id;
        $users= User::where('users.id',$id)->first();
        if($users){

			$taskfolder = Taskfolder::find($folder_id);
			$taskfolder['folder_id']=$taskfolder->id;
            $tasklistdata = Jobassignemployee::
			 where('jobassignemployee.job_employee_id',$id)
            ->where('jobassignemployee.job_id',$job_id)
			//->with('JobtaskList')
			->with(['JobtaskList' => function ($qs) use ($folder_id){
                     $qs

					   ->where('jobtask.folder_id',$folder_id);
                   }])
			->with(['checkemployeeJob' => function ($q) use ($id){
                     $q
                       ->where('employeejob.employee_id', $id);
                   }])
            ->select('jobassignemployee.id as relation_id','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id','jobassignemployee.status as status','jobassignemployee.created_at as created_at','jobassignemployee.updated_at as updated_at')
            ->get();

			 $finalArray = array();
            if(count($tasklistdata)>0){
				// $data=$tasklistdata;
                // $message = 'Success';
				foreach($tasklistdata as $p_key => $p_val){
                    $finalArray['JobtaskList'][$p_key] = $p_val;
					$finalArray['folder'] = $taskfolder;
                    $message = 'Success';
                }
            }
            else{
                $status = false;
                $message = 'No Job Task Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$finalArray,'message'=>$message,'code'=> $code]);
    }

    public function jobfolderlist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$id)->first();
        if($users){

        $folderlistdata = Jobassignemployee::with('JobfolderList')
            ->where('jobassignemployee.job_employee_id',$id)
            ->where('jobassignemployee.job_id',$job_id)
            ->select('jobassignemployee.id as relation_id','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id','jobassignemployee.status as status','jobassignemployee.created_at as created_at','jobassignemployee.updated_at as updated_at')
            ->get();
            if(count($folderlistdata)>0){
                 $data=$folderlistdata;
                 $message = 'Success';

            }
            else{
                $status = false;
                $message = 'Job is not Assign';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

	public function jobtaskfolderlist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$id)->first();
        if($users){

        $folderlistdata = Jobassignemployee::with('JobtaskfolderList')
            ->where('jobassignemployee.job_employee_id',$id)
            ->where('jobassignemployee.job_id',$job_id)
            ->select('jobassignemployee.id as relation_id','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id','jobassignemployee.status as status','jobassignemployee.created_at as created_at','jobassignemployee.updated_at as updated_at')
            ->get();
            if(count($folderlistdata)>0){
                 $data=$folderlistdata;
                 $message = 'Success';

            }
            else{
                $status = false;
                $message = 'Job is not Assign';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    public function foldervideodetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $users= User::where('users.id',$id)->first();
        if($users){
          $folder = Folder::find($folder_id);
           $folder['folder_id']= $folder->id;
          $foldervideolistdata = Foldervideo::join('videos','videos.id','=','foldervideo.video_id')
          ->where('videos.status','=','active')
          ->where('foldervideo.folder_id',$folder_id)
          ->where('foldervideo.job_id',$job_id)
          ->select('foldervideo.id as relation_id','foldervideo.folder_id as folder_id','foldervideo.job_id as job_id','foldervideo.video_id as video_id','foldervideo.status as status','foldervideo.user_type as user_type','foldervideo.employee_id as employee_id','videos.title as title','videos.description as description','videos.video_url as video_url','videos.created_by as created_by')
          ->get();
          $finalArray = array();
            if(count($foldervideolistdata)>0){
                foreach($foldervideolistdata as $p_key => $p_val){
                    $finalArray['video_detail'][$p_key] = $p_val;
                    $finalArray['video_detail'][$p_key]['video_url']=url('/Video'.'/'.$p_val['video_url']);
					//$finalArray['folder'] = $folder;
                    $message = 'Success';
                }
            }
            else{
                $status = false;
                $message = 'No Folders Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$finalArray,'message'=>$message,'code'=> $code]);
    }

    public function folderdocumentdetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $users= User::where('users.id',$id)->first();
        if($users){
          $folder = Folder::find($folder_id);
          $folder['folder_id']=$folder->id;
          $folderdocumentlistdata = Folderdocument::join('documents','documents.id','=','folderdocument.document_id')
          ->where('documents.status','=','active')
          ->where('folderdocument.folder_id',$folder_id)
          ->where('folderdocument.job_id',$job_id)
           ->select('folderdocument.id as relation_id','folderdocument.folder_id as folder_id','folderdocument.job_id as job_id','folderdocument.document_id as document_id','folderdocument.status as status','folderdocument.user_type as user_type','folderdocument.employee_id as employee_id','documents.title as title','documents.description as description','documents.document_url as document_url','documents.created_by as created_by')
          ->get();
          $finalArray = array();
            if(count($folderdocumentlistdata)>0){
                foreach($folderdocumentlistdata as $p_key => $p_val){
                    $finalArray['document_detail'][$p_key] = $p_val;
                    $finalArray['document_detail'][$p_key]['document_url']=url('/Document'.'/'.$p_val['document_url']);
					//$finalArray['folder'] = $folder;
                    $message = 'Success';
                }
            }
            else{
                $status = false;
                $message = 'No Folders Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$finalArray,'message'=>$message,'code'=> $code]);
    }

    public function videodetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $video_id = $request->video_id;
        $users= User::where('users.id',$id)->first();
        if($users){
          $folder = Folder::find($folder_id);
          // $folder['folder_id']= $folder->id;
          $foldervideolistdata = Video::where('id',$video_id)
          ->select('videos.id as relation_id','videos.title as title','videos.description as description','videos.video_url as video_url','videos.status as status','videos.deleted_at as deleted_at','videos.user_type as user_type')
          ->get();

          $finalArray = array();
            if((count($foldervideolistdata) > 0)){
                foreach($foldervideolistdata as $p_key => $p_val){
                    $finalArray['video_detail'][$p_key] = $p_val;
                    $finalArray['video_detail'][$p_key]['video_url'] = url('/Video'.'/'.$p_val['video_url']);
                    //$finalArray['folder'] = $folder;
                    $message = 'Success';
                }

            }
            else{
                $status = false;
                $message = 'No Videos Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$finalArray,'message'=>$message,'code'=> $code]);
    }

     public function documentdetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $document_id = $request->document_id;
        $users= User::where('users.id',$id)->first();
        if($users){
          $folder = Folder::find($folder_id);
          $folder['folder_id']=$folder->id;
          $documentdetaildata = Document::where('id',$document_id)
          ->select('documents.id as relation_id','documents.title as title','documents.description as description','documents.document_url as document_url','documents.status as status','documents.deleted_at as deleted_at','documents.user_type as user_type')
          ->get();
          $finalArray = array();
            if(count($documentdetaildata)>0){
                foreach($documentdetaildata as $p_key => $p_val){
                    $finalArray['document_detail'][$p_key] = $p_val;
                    $finalArray['document_detail'][$p_key]['document_url']=url('/Document'.'/'.$p_val['document_url']);
                    //$finalArray['folder'] = $folder;
                    $message = 'Success';
                }

            }
            else{
                $status = false;
                $message = 'No Document Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$finalArray,'message'=>$message,'code'=> $code]);
    }
    public function createjobcard(Request $request){
        $data = [];
        $message = "Job card Saved";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $job_title = $request->job_title;
        $data = $request->data;
        $signature_img=$request->image;
        $second_client_name=$request->second_client_name;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if($signature_img==''){
            $status = false;
            $message = 'Select Image';
            $code = 400;
        }
        if($data==''){
            $status = false;
            $message = 'Enter Data';
            $code = 400;
        }
        if($job_title==''){
            $status = false;
            $message = 'Enter Job Title';
            $code = 400;
        }
        $jobcarddata=json_decode($data);

        if($status){
            $total_amount=0;
            $jobcard = new Jobcard;
            $jobcard->job_id=$job_id;
            $jobcard->job_title=$job_title;
            $jobcard->user_id=$user_id;
            $jobcard->second_client_name=$second_client_name;
            if ($request->image) {
                $job_card_signature_image = $request->image;
                $filename = uniqid(time()) . '.' . $job_card_signature_image->getClientOriginalExtension();
                $job_card_signature_image->move(public_path('CardsSignature'), $filename);
                $jobcard['job_card_signature_image'] = $filename;

            }

            $jobcard->save();


            foreach($jobcarddata->data as $val){
                if($val->name!='' && $val->qty!='' && $val->price!=''){
                    $data = new Jobcarddetail;

                    $data['job_card_id'] = $jobcard->id;
                    $data['job_card_name'] = $val->name;
                    $data['job_card_qty'] = $val->qty;
                    $data['job_card_price'] = $val->price;
                    $data['job_card_status'] = 'active';
                    $data['total']=($val->qty)*( $val->price);
                    $total_amount=$total_amount+$val->qty*$val->price;
                    $data->save();
                }
            }

            $jobcard->total_amount=$total_amount;
            $jobcard->save();
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }
    public function jobcardlist(Request $request){
        $data = [];
        $message = "Success";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$id)->first();
        $job= Job::where('job.id',$job_id)->first();
		$jobcardexist= Jobcard::where('jobcard.job_id',$job_id)->first();

        if($users){
            if($job){
				if($jobcardexist){

					$jobcard = Jobcard::with('jobdetailList')->where('job_id',$job_id)->where('user_id',$id)->select('jobcard.id as job_card_id','jobcard.job_id as job_id','jobcard.job_title as job_title','jobcard.user_id as user_id','jobcard.total_amount as total_amount','jobcard.job_card_signature_image as job_card_signature_image','jobcard.status as status','jobcard.second_client_name as second_client_name','jobcard.id as id')->get();
					$finalArray = array();
					$data['job_card']=$jobcard;
				}
				else{
					$status = false;
					$message = 'No Job Card Found';
					$code = 400;
				}
			}
            else{
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }
        }else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

     public function jobcarddetail(Request $request){
        $data = [];
        $message = "Success";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $job_card_id = $request->job_card_id;
        $users= User::where('users.id',$id)->first();
        $job= Job::where('job.id',$job_id)->first();

        if($users){
            if($job){
                $jobcard = Jobcard::with('jobdetailList')->where('id',$job_card_id)->where('user_id',$id)->select('jobcard.id as job_card_id','jobcard.job_id as job_id','jobcard.job_title as job_title','jobcard.user_id as user_id','jobcard.total_amount as total_amount','jobcard.job_card_signature_image as job_card_signature_image','jobcard.status as status','jobcard.second_client_name as second_client_name','jobcard.id as id')->get();
				$data['job_card']=$jobcard;
            }
            else{
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }
        }else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }
    public function addimage(Request $request){
        $data = [];
        $message = "Folder Created";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $folder_name = $request->folder_name;
        $folder_img=$request->image;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if($folder_name==''){
            $status = false;
            $message = 'Enter Folder Name';
            $code = 400;
        }
        if($folder_img==''){
            $status = false;
            $message = 'Upload Folder Image';
            $code = 400;
        }

        if($status){
            $folder = new Folder;
            $folder->job_id=$job_id;
            $folder->folder_name=$folder_name;
            $folder->user_type='employee';
            $folder->employee_id=$user_id;
            if ($request->image) {
                $folder_image = $request->image;
                $filename = uniqid(time()) . '.' . $folder_image->getClientOriginalExtension();
                $folder_image->move(public_path('FolderImage'), $filename);
                $folder['folder_img'] = url('FolderImage').'/'.$filename;
            }

            $folder->save();

            $folder->created_by=$folder->id;
            $folder->save();
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }


     public function adddocument(Request $request){
        $data = [];
        $message = "Document Uploaded";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $document_title = $request->document_title;
        $document_description = $request->document_description;
        $document_img=$request->image;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();
        $folder= Folder::where('folder.id',$folder_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if(!$folder){
            $status = false;
            $message = 'No Folder Found';
            $code = 400;
        }else if($folder==''){
            $status = false;
            $message = 'Enter Folder Id';
            $code = 400;
        }
        if($document_title==''){
            $status = false;
            $message = 'Enter Document title';
            $code = 400;
        }
        if($document_img==''){
            $status = false;
            $message = 'Upload Document';
            $code = 400;
        }

        if($status){
            $document = new Document;
            $document->title=$document_title;
            $document->description=$document_description;
            $document->user_type='employee';
            if ($request->image) {
                $document_image = $request->image;
                $filename = uniqid(time()) . '.' . $document_image->getClientOriginalExtension();
                $document_image->move(public_path('Document'), $filename);
                $document['document_url'] = $filename;
            }
            $document->save();

            $folderdocument = new Folderdocument;
            $folderdocument->folder_id=$folder_id;
            $folderdocument->job_id=$job_id;
            $folderdocument->document_id=$document->id;
            $folderdocument->user_type='employee';
            $folderdocument->employee_id=$user_id;
            $folderdocument->save();

        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function addvideo(Request $request){
        $data = [];
        $message = "Video Uploaded";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $video_title = $request->video_title;
        $video_description = $request->video_description;
        $video_img=$request->video;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();
        $folder= Folder::where('folder.id',$folder_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if(!$folder){
            $status = false;
            $message = 'No Folder Found';
            $code = 400;
        }else if($folder==''){
            $status = false;
            $message = 'Enter Folder Id';
            $code = 400;
        }
        if($video_title==''){
            $status = false;
            $message = 'Enter Video title';
            $code = 400;
        }
        if($video_img==''){
            $status = false;
            $message = 'Upload Video';
            $code = 400;
        }

        if($status){
            $video = new Video;
            $video->title=$video_title;
            $video->description=$video_description;
            $video->user_type='employee';
            if ($request->video) {
                $video_image = $request->video;
                $filename = uniqid(time()) . '.' . $video_image->getClientOriginalExtension();
                $video_image->move(public_path('Video'), $filename);
                $video['video_url'] = $filename;
            }
            $video->save();

            $foldervideo = new Foldervideo;
            $foldervideo->folder_id=$folder_id;
            $foldervideo->job_id=$job_id;
            $foldervideo->video_id=$video->id;
            $foldervideo->user_type='employee';
            $foldervideo->employee_id=$user_id;
            $foldervideo->save();

        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }
    public function updatejobtask(Request $request){
        $data = [];
        $message = "Job Task Updated";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $jobtask_id = $request->jobtask_id;
        $jobtask_status = $request->jobtask_status;
        $users= User::where('users.id',$user_id)->first();

        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if($jobtask_id==''){
            $status = false;
            $message = 'Enter Job Task Id';
            $code = 400;
        }
        if($jobtask_status==''){
            $status = false;
            $message = 'Enter Job Task Status';
            $code = 400;
        }
       $jobtask = Jobtask::find($jobtask_id);
        if($status){
            if($users){
                if($jobtask){
                    $jobtask->jobtask_status=$jobtask_status;
                    $jobtask->save();
                }else{
                    $status = false;
                    $message = 'No Job Task Found';
                    $code = 400;
                }
            }
            else{
                    $status = false;
                    $message = 'No User Found';
                    $code = 400;
                }
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }
     public function markasdone(Request $request){
        $data = [];
        $message = "Job Status Updated";
        $status = true;
        $code = 200;
        $subject="Job Status";
        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$user_id)->first();

        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }

       $job = Job::find($job_id);
        if($status){
            if($users){
                if($job){
                    $job->job_status='markasdone';
                    $job->save();
					$this->mail_function->sendMailJobAction($job_id);
                }else{
                    $status = false;
                    $message = 'No Job Found';
                    $code = 400;
                }
            }
            else{
                    $status = false;
                    $message = 'No User Found';
                    $code = 400;
                }
        }


        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }
    public function addnotes(Request $request){
        $data = [];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $notes = $request->notes;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if($notes==''){
            $status = false;
            $message = 'Enter Notes';
            $code = 400;
        }

        if($status){
            $foldernotes = new Foldernotes;
            $foldernotes->job_id=$job_id;
            $foldernotes->user_type='employee';
            $foldernotes->employee_id=$user_id;
            $foldernotes->notes=$notes;
            $foldernotes->save();
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }

     public function notesfolderlist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$id)->first();
        if($users){

        $foldernoteslistdata = Foldernotes::where('status','active')->where('job_id',$job_id)->select('id as notes_id','job_id as job_id','notes as notes','user_type as user_type','employee_id as employee_id','status as status','deleted_at as deleted_at')->get();
            if(count($foldernoteslistdata)>0){
                 $data=$foldernoteslistdata;
                 $message = 'Success';

            }
            else{
                $status = false;
                $message = 'Job is not Assign';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

	public function editnotes(Request $request){
        $data = [];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $note_id=$request->note_id;
        $notes = $request->notes;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();
        $foldernotes= Foldernotes::where('foldernotes.id',$note_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if(!$foldernotes){
            $status = false;
            $message = 'No Notes Found';
            $code = 400;
        }else if($foldernotes==''){
            $status = false;
            $message = 'Enter Notes Id';
            $code = 400;
        }
        if($notes==''){
            $status = false;
            $message = 'Enter Notes';
            $code = 400;
        }

        if($status){
            //$foldernotes = new Foldernotes;
            $foldernotes->job_id=$job_id;
            $foldernotes->user_type='employee';
            $foldernotes->employee_id=$user_id;
            $foldernotes->notes=$notes;
            $foldernotes->save();
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function deletenotes(Request $request){
        $data = [];
        $message = "Deleted Successfully";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $note_id=$request->note_id;
        $users= User::where('users.id',$user_id)->first();
        $foldernotes= Foldernotes::where('foldernotes.id',$note_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$foldernotes){
            $status = false;
            $message = 'No Notes Found';
            $code = 400;
        }else if($foldernotes==''){
            $status = false;
            $message = 'Enter Notes Id';
            $code = 400;
        }

        if($status){
            $foldernotes->delete();
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }

	public function jobsDoneList(Request $request){


        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $page = $request->page;
        $users= User::where('users.id',$id)->first();
        if($users){
            $joblistdata = Job::join('jobassignemployee','job.id','=','jobassignemployee.job_id')
            ->where('jobassignemployee.job_employee_id',$id)
			->where('job.job_status','markasdone')
            ->select('job.id as relation_id','job.title as title','job.description as description','job.duedate as duedate','job.notes as notes','job.status as status','job.deleted_at as deleted_at','job.created_by as created_by','job.created_at as created_at','job.updated_at as updated_at','job.client_id as client_id','job.job_status as job_status','job.job_number as job_number','job.start_time as start_time','job.end_time as end_time','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id');

            if ($request->search != ''){
                $search = $request->search;
                $where_filter = "(job.title LIKE '%$search%' OR job.job_number LIKE  '%$search%' OR job.duedate LIKE  '%$search%' )";
                $joblistdata = $joblistdata->whereRaw($where_filter);

                //$joblistdata->where('job.title', 'like', $search.'%');
            }
            $joblistdata  = $joblistdata->paginate(10);

            if(count($joblistdata)>0){
                $data=$joblistdata;
            }
            else{
                $status = false;
                $message = 'No Jobs Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    public function addJobGeothermalForm(Request $request){
        $data = [];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
        );
        if ($request->install_date != null) {
            $rules['install_date'] = 'required|date|date_format:Y-m-d';
        }
        if ($request->desuperheater != null) {
            $rules['desuperheater'] = 'required|in:Y,N';
        }

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();
            $jobGeothermalForm = JobGeothermalForm::where('job_id', $request->job_id)->where('status', 'active')->first();


            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            else if($jobGeothermalForm != null){
                $status = false;
                $message = 'Already Added form for this job';
                $code = 400;
            }

            if($status){
                $requestData = $request->except('user_id','api_token');
                $requestData['created_by'] = $user_id;

                $data = JobGeothermalForm::create($requestData);

            }
        }
        $data = (object) $data;
        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function jobGeothermalFormList(Request $request){
        $data = [];
        $message = "Success";
        $status = true;
        $flag = 0;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 0;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 0;

            }


            if($status){
                $data = JobGeothermalForm::select('job_geothermal_form.*','job.title as job_name')->join('job','job_geothermal_form.job_id','job.id')->where('job_geothermal_form.job_id',$job_id)->first();

                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = [];
                    $flag = 0;
                }
                else {
                    $resArr = array();
                    $resArr['job_information'] = array('job_name' => $data->job_name, 'model' => $data->model,
                        'job_geothermal_form_id' => $data->id,
                        'serial' => $data->serial,
                        'install_date' => $data->install_date,
                        'open_closed_loop' => $data->open_closed_loop,
                        'desuperheater' => $data->desuperheater);
                    $resArr['flow_rate_in_gpm'] = array(
                        'water_in_pressure_source_coax' => $data->source_coax_a,
                        'water_out_pressure_source_coax' => $data->source_coax_b,
                        'pressure_drop_source_coax' => $data->source_coax_c,
                        'rate_in_table_source_coax' => $data->source_coax_d,

                        'water_in_pressure_load_coax' => $data->load_coax_a,
                        'water_out_pressure_load_coax' => $data->load_coax_b,
                        'pressure_drop_load_coax' => $data->load_coax_c,
                        'rate_in_table_load_coax' => $data->load_coax_d);
                    $resArr['temperature_rise_drop_across_coaxial_heat_exchanger'] = array(
                        'water_in_temperature_cooling' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_cooling_e,
                        'water_out_temperature_cooling' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_cooling_f,
                        'temperature_difference_cooling' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_cooling_g,

                        'water_in_temperature_heating' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_heating_e,
                        'water_out_temperature_heating' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_heating_f,
                        'temperature_difference_heating' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_heating_g);

                    $resArr['temperature_rise_drop_across_air_coil'] = array(
                        'suppy_air_temperature_cooling' => $data->temperature_rise_drop_across_air_coil_cooling_h,
                        'return_air_temperature_cooling' => $data->temperature_rise_drop_across_air_coil_cooling_i,
                        'temperature_difference_cooling' => $data->temperature_rise_drop_across_air_coil_cooling_j,

                        'suppy_air_temperature_heating' => $data->temperature_rise_drop_across_air_coil_heating_h,
                        'return_air_temperature_heating' => $data->temperature_rise_drop_across_air_coil_heating_i,
                        'temperature_difference_heating' => $data->temperature_rise_drop_across_air_coil_heating_j);

                    $resArr['temperature_rise_drop_across_air_coil_load_coax'] = array(
                        'lwt_cooling' => $data->load_coax_cooling_h,
                        'ewt_cooling_1' => $data->load_coax_cooling_i,
                        'ewt_cooling_2' => $data->load_coax_cooling_j,

                        'lwt_heating' => $data->load_coax_heating_h,
                        'ewt_heating_1' => $data->load_coax_heating_i,
                        'ewt_heating_2' => $data->load_coax_heating_j);

                    $resArr['HR_HE'] = array(
                        'brine_factor' => $data->HR_HE_brine_factor_k,
                        'HR_HE_cooling' => $data->HR_cooling_i,
                        'HR_HE_heating' => $data->HE_heating_i);

                    $resArr['watts'] = array(
                        'volts_cooling' => $data->watts_cooling_m,
                        'total_amps_cooling' => $data->watts_cooling_n,
                        'watts_cooling' => $data->watts_cooling_o,

                        'volts_heating' => $data->watts_heating_m,
                        'total_amps_heating' => $data->watts_heating_n,
                        'watts_heating' => $data->watts_heating_o);

                    $resArr['capacity'] = array(
                        'cooling_capacity' => $data->capacity_cooling_p,
                        'heating_capacity' => $data->capacity_heating_p);

                    $resArr['efficiency'] = array(
                        'cooling_EER' => $data->efficiency_cooling_q,
                        'heating_COP' => $data->efficiency_heating_q);

                    $resArr['SH_SC_cooling'] = array(
                        'suction_pressure' => $data->SH_SC_cooling_r,
                        'suction_saturation_temperature' => $data->SH_SC_cooling_s,
                        'suction_line_temperature' => $data->SH_SC_cooling_t,
                        'SH' => $data->SH_SC_cooling_u,
                        'head_pressure' => $data->SH_SC_cooling_v,
                        'high_pressure_saturation_temperature' => $data->SH_SC_cooling_w,
                        'liquid_line_temperature' => $data->SH_SC_cooling_x);

                    $resArr['SH_SC_heating'] = array(
                        'suction_pressure' => $data->SH_SC_heating_r,
                        'suction_saturation_temperature' => $data->SH_SC_heating_s,
                        'suction_line_temperature' => $data->SH_SC_heating_t,
                        'SH' => $data->SH_SC_heating_u,
                        'head_pressure' => $data->SH_SC_heating_v,
                        'high_pressure_saturation_temperature' => $data->SH_SC_heating_w,
                        'liquid_line_temperature' => $data->SH_SC_heating_x);

                        $flag = 1;
                        $data = $resArr;
                }
            }
        }
        $data = (object)$data;
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function editJobGeothermalForm(Request $request){
        $data = [];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'job_geothermal_form_id' => 'required|integer',
            'user_id' => 'required|integer',
            'job_id' => 'required|integer',
        );

        if ($request->install_date != null) {
            $rules['install_date'] = 'required|date|date_format:Y-m-d';
        }
        if ($request->desuperheater != null) {
            $rules['desuperheater'] = 'required|in:Y,N';
        }

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $jobGeothermalForm = JobGeothermalForm::where('id', $request->job_geothermal_form_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($jobGeothermalForm == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {
                $requestData = $request->except('user_id', 'api_token','job_geothermal_form_id','job_id');
                $data = $jobGeothermalForm->update($requestData);
            }
        }
        $data = (object) $data;

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }
}


