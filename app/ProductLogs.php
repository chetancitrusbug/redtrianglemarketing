<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductLogs extends Model
{
    protected $table = 'product_logs';
    protected $fillable = [
        'product_id', 'user_id','start_datetime','end_datetime'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
