<!DOCTYPE html>
<html>
	<head>
		<title>NGN Construction</title>
		<style>
			h2 {
				color:#BF1E2E;
				font-size: 20px;
				text-align: center;
			}
			table {
				border-collapse: collapse;
				width: 50%;
				text-align: center;
				margin:0px auto 20px auto;

			}

			th, td {
				text-align: left;
				padding: 8px;
				width: 50%;
				font-size: 15px;
			}

			tr:nth-child(odd){background-color: #f2f2f2}

			th {
				background-color: #4CAF50;
				color: white;
			}

			p {
				margin:30px;
				font-size:15px;
			}

		</style>
	</head>
	<body style="margin:30px auto; width:700px;">

		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
            <p>hello admin,</p>
            <h2>Stock Remainder<h2>
			<table>

				<tr>
                    <td>Stock Left:</td>
                    <td>{{ $value }}</td>

                </tr>
                <tr>
                    <td colspan="2"><a href="{{ url('/admin/material/'.$materialdata->id.'/edit') }}">Click here for add Stock</a></td>
                </tr>

			</table>
		</div>

	</body>
</html>