<?php

return [

	'add_document' => 'Add Document',
    'add_new_document' => 'Add New Document',
    'back' => 'Back',
    'edit_document' => 'Edit Document',
    'update' => 'Update',
    'title' => 'Title',
	'documents' => 'Documents',
    'id' => 'Id',
	'status' => 'Status',
	'actions' => 'Actions',
	'active' => 'Active',
    'inactive' => 'Inactive',
    'blocked' => 'Blocked',
	'view' => 'View',
	'document' => 'Document',
	'create' => 'Create',
	'document_url' => 'Document Url',
	'description' => 'Description',
	'changedocument'=>'Change Document',
	'adddocument'=>'Document',
	'view_document'=>'View Document',
];