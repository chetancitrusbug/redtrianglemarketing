
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', trans('user.name'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(!(Route::currentRouteName() == 'users.edit'))
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email',trans('user.email'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('password',trans('user.password'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@else
	<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email',trans('user.email'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control','readonly']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif
@if(Auth::user()->id==1)
<div class="form-group{{ $errors->has('notes') ? ' has-error' : ''}}">
    {!! Form::label('notes',trans('user.notes'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
         {!! Form::textarea('notes', null, ['class' => 'form-control','size' => '30x5']) !!}
        {!! $errors->first('notes', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
@endif
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('user.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@push('script-head')

<script type="text/javascript">
 
    $("formUser").validate({
        //var cpf = $('#cpf').val();
        //cpf=cpf.replace(".","");
        //cpf = cpf.replace("-","");
        //cpf= cpf.replace(".","");
        //alert(cpf);
        rules: { 
          
            name:{
                required:true
            },
            email:{
                required:true,
                email: true
            },
            password:{
             //    required:true,
             //   pwcheck: true
            },
            
        },
        messages: {
            
            name:{
                required:"Name Is required"
            },
            email:{
                required:"Email is required",
                email: "Enter Valid Email address"
            },
            password:{
               // required:"Password is required",
               // pwcheck: "Password must be contain 1 uppercase, 1 lowercase and 1 number value"
            },
            

        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $.validator.addMethod("pwcheck", function(value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            && /[a-z]/.test(value) // has a lowercase letter
            && /\d/.test(value) // has a digit
    });
    
   
</script>
@endpush

